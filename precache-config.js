let SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');

module.exports = {
    navigateFallback: '/index.html',
    navigateFallbackWhitelist: [/^(?!\/__)/], // <-- necessary for Firebase OAuth
    stripPrefix: 'dist/noter-client',
    root: 'dist/noter-client/',
    plugins: [
        new SWPrecacheWebpackPlugin({
          cacheId: 'firestarter',
          filename: 'service-worker.js',
          staticFileGlobs: [
            'dist/noter-client/index.html',
            'dist/noter-client/**.js',
            'dist/noter-client/**.css'
          ],
          stripPrefix: 'dist/noter-client/assets/',
          mergeStaticsConfig: true // if you don't set this to true, you won't see any webpack-emitted assets in your serviceworker config
        }),
      ]
  };