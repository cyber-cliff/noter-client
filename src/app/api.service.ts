import { Injectable } from '@angular/core';
import { EventEmitterService } from './event-emitter.service';
import { VirtualTimeScheduler } from 'rxjs';
import { LogService } from './services/log.service';
import { SettingsService } from './services/settings.service';
import { OverlayService } from './shell/overlays/overlay.service';
import { StateService } from './services/state.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  session = {
    token: '',
    user_id: ''
  };

  username = '';

  serverData: any; // Stores data got about server info to prevent spamming the API for 2 seconds

  BASE_API_ADDRESS = localStorage.overwriteApiBase || 'https://noter-server.eu-gb.mybluemix.net/api/v2/';

  initLoggingIn = true;
  loggingIn = false;
  failedAutoLogin = false;

  startTimes = [];
  recentReq = '';

  // If this is ever above 3 we will auto-fail any request with an auth error without reauthing since we don't want an infinite loop
  authErrorCounter = 0;

  // For visual reasons, everything else is a down route
  // Upload endpoint is defined as an endpoint that has the primary function of
  // a) sending data so that server deals with it or stores it (ex. update note)
  // or b) sends signal to make and action happen on the server (ex. delete a note)
  // In both cases, the server should not give other response than the action successful/failed
  UPLOAD_ENDPOINTS = [
    'auth/login',
    'auth/register',
    'auth/destroy',

    'users/saved/save',
    'users/saved/remove',

    'notes/create',
    'notes/update',
    'notes/delete',

    'tags/create',
    'tags/delete',

    'tasks/create',
    'tasks/update'
  ];

  constructor(
    private eventEmitter: EventEmitterService,
    private log: LogService,
    private overlayService: OverlayService,
    private settings: SettingsService,
    private stateService: StateService
  ) { }

  async pushServerInfoToLog() {
    let serverData;

    try {
      serverData = await this.request('info/about', {});

      if (!serverData.success) {
        throw ('Not success');
      }
    } catch (e) {
      this.log.pushLog({
        type: 'fail',
        msg: `Initial server check failed`
      });
      return;
    }

    this.log.pushLog({
      type: 'debug',
      msg: `Server version: ${serverData.data.version.string} [${serverData.data.version.code}]`
    });
  }

  async register(username, password) {
    const res = await this.request('auth/register', { username, password });

    if (res.success) {
      this.overlayService.cancelOverlay();
    }

    return res;
  }

  async manualLogin(username, password) {
    const res = await this.request('auth/login', { username, key: password, generate_persistent_token: true });

    if (res.success) {
      // Store username
      this.username = username;

      // Session related
      this.session.token = res.data.token;
      this.session.user_id = res.data.user_id;

      // Store session in sessionStorage
      sessionStorage.session = JSON.stringify(this.session);
      sessionStorage.username = username;

      // Store PT in localStorage
      localStorage.n_pt = res.data.persistent_token;
      localStorage.n_un = username;

      // Emit events
      this.eventEmitter.onLogIn();
      this.eventEmitter.loginStatusChangeEmitter.emit({ loggedIn: true });
      this.overlayService.cancelOverlay(); // Since it's manual it must have come from the overlay, so we close it after manual login
    }

    return res;
  }

  async ptLogin(username, pt) {
    const res = await this.request('auth/login', { username, key: pt, is_persistent_token: true });

    if (res.success) {
      this.username = username;

      this.session.token = res.data.token;
      this.session.user_id = res.data.user_id;

      sessionStorage.session = JSON.stringify(this.session);
      sessionStorage.username = username;

      this.eventEmitter.onLogIn();
      this.eventEmitter.loginStatusChangeEmitter.emit({ loggedIn: true });
    }

    return res;
  }

  async autoLogin() {
    this.loggingIn = true;
    this.failedAutoLogin = false;

    this.log.pushLog({
      type: 'info',
      msg: `[AUTH] Attempting automatic login`
    });

    if (sessionStorage.session) {
      this.log.pushLog({
        type: 'info',
        msg: `[AUTH] Eligible for session restore`
      });

      const storedSession = JSON.parse(sessionStorage.session);

      let res;

      res = await this.authSession(storedSession);

      console.log('seesion auth res', res);

      if (!res.success) {
        if (res.error.fetchError) {
          this.log.pushLog({
            type: 'fail',
            msg: `[AUTH] Failed to verify session due to FE`
          });
          this.loggingIn = false;
          this.failedAutoLogin = true;
          this.initLoggingIn = false;
          return false;
        }
      }

      if (res.success) {
        if (!res.data.valid) {
          sessionStorage.removeItem('session');
          this.log.pushLog({
            type: 'warn',
            msg: `[AUTH] Session invalid! Purging session data`
          });
        } else {
          this.session = storedSession;
          this.username = sessionStorage.username;
          this.eventEmitter.onLogIn();
          this.eventEmitter.loginStatusChangeEmitter.emit({ loggedIn: true });

          this.log.pushLog({
            type: 'success',
            msg: `[AUTH] Session restored successfully`
          });

          this.loggingIn = false;
          this.initLoggingIn = false;
          return true;
        }
      }
    }


    // * Persistent login
    if (localStorage.n_un && localStorage.n_pt) {
      this.log.pushLog({
        type: 'info',
        msg: `[AUTH] Eligible for PT login`
      });

      const ptRes = await this.ptLogin(localStorage.n_un, localStorage.n_pt);

      if (ptRes.success) {
        this.log.pushLog({
          type: 'success',
          msg: `[AUTH] PT login successful`
        });

        this.loggingIn = false;
        this.initLoggingIn = false;
        return true;
      } else if (ptRes.error.code === 2 || ptRes.error.code === 3) { // If it was an invalid username / password
        this.log.pushLog({
          type: 'fail',
          msg: `[AUTH] PT information invalid. Purging it from storage`
        });

        // Purge everything
        localStorage.removeItem('n_un');
        localStorage.removeItem('n_pt');
        this.loggingIn = false;
        this.initLoggingIn = false;
      } else {
        this.log.pushLog({
          type: 'fail',
          msg: `[AUTH] PT login failed`
        });
        this.loggingIn = false;
        this.failedAutoLogin = true;
        this.initLoggingIn = false;
      }
    }

    this.loggingIn = false;
    this.initLoggingIn = false;
    return false;
  }

  async authSession(session?: any) {
    return await this.request('auth/validate', (session) ? session : this.session, false);
  }

  logOut() {
    this.session.token = '';
    this.session.user_id = '';
    this.username = '';

    sessionStorage.removeItem('session');
    sessionStorage.removeItem('username');
    localStorage.removeItem('n_un');
    localStorage.removeItem('n_pt');

    this.eventEmitter.onLogOut();
    this.eventEmitter.loginStatusChangeEmitter.emit({ loggedIn: false });

    this.eventEmitter.pushNotif({
      icon: 'fas fa-hand-paper',
      details: 'Logged out successfully. See ya next time!',
      color: 'var(--color-green)'
    });
  }

  async request(
    endpoint: string,
    data: object,
    requiresAuth?: boolean,
    options?: {
      ignoreAuthWait?: boolean,
      skipAuthCheck?: boolean,
      disableTracking: boolean
    }
  ) {
    const requestLogId = this.logReq(endpoint, 'init');

    // If it requires auth and there is not an option for skipping the auth check
    if (requiresAuth && (!options || !options.ignoreAuthWait)) { await this.waitForAuth(); }

    let auth: any = false; // If we don't define it bellow, it won't be sent
    if (requiresAuth && this.session.token) {
      auth = this.session;

      if (!this.settings.values.privacy_active_time) {
        auth.disable_active_time = true;
      } else {
        auth.disable_active_time = undefined;
      }
    }

    let rawRes;
    try {
      rawRes = await fetch(this.BASE_API_ADDRESS + endpoint, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data,
          auth,
          diag: (this.settings.values.privacy_diag && !(options && options.disableTracking)) ? {
            client_version: this.settings.noterVersion.code,
            user_id: this.session.user_id
          } : undefined
        })
      });
      this.logReq(endpoint, 'done', requestLogId);
    } catch (e) {
      console.log('fe');
      this.logReq(endpoint, 'fail', requestLogId);
      return this.fetchError();
    }

    if (!rawRes.ok) {
      console.log('fe due to not ok');
      this.logReq(endpoint, 'fail', requestLogId);
      return this.fetchError();
    }

    const res = await rawRes.json();

    if (!options || !options.skipAuthCheck) {
      console.log(endpoint);

      const authErrCheckRes = await this.checkForAuthError(res);
      console.log(authErrCheckRes);

      if (authErrCheckRes && authErrCheckRes.fixed) {
        return this.request(endpoint, data, requiresAuth, options);
      }
      if (authErrCheckRes && !authErrCheckRes.fixed) { return { success: false, error: { authError: true } }; }
    }

    this.checkForInternalError(res);

    console.log(res);

    return res;
  }

  fetchError(showFailNotif?) {
    if (showFailNotif) {
      this.eventEmitter.pushNotif({
        icon: 'fas fa-times-circle',
        details: 'Well, that failed! Connection with server could not be established.',
        color: 'var(--color-red)'
      });
    }

    return {
      success: false,
      error: { fetchError: true },
    };
  }

  async checkForAuthError(res) {
    if (!res.success) {
      if (res.error.fetchError) { return false; } // We don't care about fetchErrors
      if (res.error.code === 4) { // 4 = auth error (session invalid)
        this.authErrorCounter++;

        this.log.pushLog({
          type: 'fail',
          msg: `Auth failed! Attempting auto login`
        });

        const isSuccessfullyLoggedIn = await this.autoLogin();

        if (isSuccessfullyLoggedIn && this.authErrorCounter < 3) {
          this.eventEmitter.pushNotif({
            icon: 'fas fa-lock-open',
            details: 'An authentication error has occurred and should be now fixed. If this keeps happening, report this issue on GitLab',
            color: 'var(--color-yellow)',
            duration: 0
          });
          return {fixed: true};
        } else {
          this.logOut();

          this.eventEmitter.pushNotif({
            icon: 'fas fa-lock',
            // tslint:disable-next-line:max-line-length
            details: 'An authentication error has occurred and could not be fixed automatically. Please try to log in again and repeat what you were trying to do.',
            color: 'var(--color-orange)',
            duration: 0
          });
          return {fixed: false};
        }
      }
    }

    return false;
  }

  async checkForInternalError(res) {
    if (!res.success) {
      if (res.error.fetchError) { return false; } // We don't care about fetchErrors
      if (res.error.code === 0) { // 4 = internal error
        this.eventEmitter.pushNotif({
          icon: 'fas fa-times-circle',
          details: `An internal server error has occurred.
                    [Please report it on GitLab](https://gitlab.com/FajsiEx/noter-client/-/issues/new?issue) and attach the following details as well as what you were trying to do: 
                    \`\`\`${res.error.e}\`\`\``,
          color: 'var(--color-red)',
          duration: 0
        });

        return true;
      }
    }

    return false;
  }

  async waitForAuth() {
    if (!this.loggingIn) {
      return;
    }

    this.log.pushLog({
      type: 'warn',
      msg: `[AUTH_AWAIT] Waiting for authentication`
    });

    await new Promise((resolve) => {
      const checkInterval = setInterval(() => {
        if (!this.loggingIn) {
          clearInterval(checkInterval);
          resolve();
        }
      }, 50);
    });

    this.log.pushLog({
      type: 'success',
      msg: `[AUTH_AWAIT] Auth resolved. Continuing request`
    });

    return;
  }

  logReq(path: string, type: string, startTimeIndex?: number) {
    switch (type) {
      case 'init':
        this.stateService.apiFetching.fetching = true;
        this.stateService.apiFetching.direction = (this.UPLOAD_ENDPOINTS.includes(path)) ? 'up' : 'down';
        this.stateService.apiFetching.endpoint = path;

        this.log.pushLog({
          type: 'info',
          msg: `[REQ] FF: ${path}`
        });

        this.recentReq = path;

        this.startTimes.push(new Date().getTime());
        return this.startTimes.length - 1;
      case 'done':
        this.stateService.apiFetching.fetching = false;
        this.stateService.apiFetching.direction = undefined;
        this.stateService.apiFetching.endpoint = undefined;

        const successDelta = new Date().getTime() - this.startTimes[startTimeIndex];

        this.log.pushLog({
          type: 'success',
          msg: `[REQ] DF: ${path} (${successDelta}ms)`
        });
        break;
      case 'fail':
        this.stateService.apiFetching.fetching = false;
        this.stateService.apiFetching.endpoint = undefined;

        const failDelta = new Date().getTime() - this.startTimes[startTimeIndex];

        this.log.pushLog({
          type: 'fail',
          msg: `[REQ] XF: ${path} (${failDelta}ms)`
        });
        break;
      default:
        this.log.pushLog({
          type: 'fail',
          msg: `[REQ] Dlog type not vaild on ${path}//${type}`
        });
    }
  }
}
