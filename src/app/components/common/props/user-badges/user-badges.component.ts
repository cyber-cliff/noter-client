import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-user-badges',
    templateUrl: './user-badges.component.html',
    styleUrls: ['./user-badges.component.scss']
})
export class UserBadgesComponent implements OnInit {

    @Input()
    badges: Array<string>;

    constructor() {
    }

    ngOnInit(): void {
    }

}
