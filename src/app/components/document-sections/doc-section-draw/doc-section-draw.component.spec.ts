import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocSectionDrawComponent } from './doc-section-draw.component';

import { RouterTestingModule } from '@angular/router/testing';

describe('DocSectionDrawComponent', () => {
  let component: DocSectionDrawComponent;
  let fixture: ComponentFixture<DocSectionDrawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocSectionDrawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocSectionDrawComponent);
    component = fixture.componentInstance;
    component.section = {
      content: '',
      type: 'draw'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
