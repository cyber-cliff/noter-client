import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { DrawCoderService } from 'src/app/services/draw-coder.service';

@Component({
  selector: 'app-doc-section-draw',
  templateUrl: './doc-section-draw.component.html',
  styleUrls: ['./doc-section-draw.component.scss']
})
export class DocSectionDrawComponent implements OnInit {

  @ViewChild('canvas') canvasElement: ElementRef;

  @Input()
  readOnly = false;

  @Input()
  section: {
    type: string,
    content: string
  };

  @Output()
  changeEvent = new EventEmitter<any>();

  canvasSafeSpace = 128;
  
  spad: any;

  constructor(private drawCoder: DrawCoderService) { }

  ngOnInit() {
    setTimeout(() => {
      this.spad = new window['SignaturePad'](this.canvasElement.nativeElement);
      this.spad.penColor = '#fff';
      this.spad.minDistance = 2;
      this.spad.onEnd = () => {
        this.onDraw();
      };

      this.resizeCanvas();
      window.addEventListener("resize", this.resizeCanvas);

      const drawData = this.drawCoder.decodeDrawData(this.section.content);

      if (this.readOnly) {
        this.canvasSafeSpace = 16;
      }

      const extendAmount = this.shouldBeExtended(drawData);
      if (extendAmount > 0) {
        this.extendCanvas(extendAmount);
      }

      this.spad.fromData(drawData);

      if (this.readOnly) {
        this.spad.off();
      }
    }, 1);
  }

  onDraw() {
    const spadData = this.spad.toData();
    this.section.content = this.drawCoder.codeDrawData(spadData);

    const extendAmount = this.shouldBeExtended(spadData);
    if (extendAmount > 0) {
      this.extendCanvas(extendAmount);
      const drawData = this.drawCoder.decodeDrawData(this.section.content);
      this.spad.fromData(drawData);
    }

    this.changeEvent.emit({
      content: this.section.content
    });
  }

  resizeCanvas() {
    const ratio = Math.max(window.devicePixelRatio || 1, 1);
    this.canvasElement.nativeElement.width = this.canvasElement.nativeElement.offsetWidth * ratio;
    this.canvasElement.nativeElement.height = this.canvasElement.nativeElement.offsetHeight * ratio;
    this.canvasElement.nativeElement.getContext("2d").scale(ratio, ratio);
    this.spad.clear(); // otherwise isEmpty() might return incorrect value
  }

  extendCanvas(amount: number) {
    this.canvasElement.nativeElement.height += amount;
  }

  // Returns 0 if no and >0 if yes and how much should it be extended by
  shouldBeExtended(drawArray) {
    const canvasHeight = this.canvasElement.nativeElement.height;
    const drawingHeight = this.getDrawingHeight(drawArray);
    const canvasSafeSpace = this.canvasSafeSpace; // Distance from the bottom that should be kept at all times whitespace

    const distanceOfDrawingFromBottom = canvasHeight - drawingHeight;
    return (distanceOfDrawingFromBottom < canvasSafeSpace) ? canvasSafeSpace - distanceOfDrawingFromBottom : 0;
  }

  // Gets the maximum y point on the spad
  getDrawingHeight(drawArray): number {
    let maxY = 0;

    for (let entryArray of drawArray) {
      for (let entry of entryArray) {
        if (entry.y > maxY) {
          maxY = entry.y;
        }
      }
    }

    return maxY;
  }

  undo() {
    const data = this.spad.toData();
    if (data) {
      data.pop(); // remove the last dot or line
      this.onDraw();

      const drawData = this.drawCoder.decodeDrawData(this.section.content);
      this.spad.fromData(drawData);

      this.canvasElement.nativeElement.height = this.canvasSafeSpace;
      this.onDraw();
    }
  }

}
