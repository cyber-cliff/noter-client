import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocSectionMarkdownComponent } from './doc-section-markdown.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('DocSectionMarkdownComponent', () => {
  let component: DocSectionMarkdownComponent;
  let fixture: ComponentFixture<DocSectionMarkdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [ DocSectionMarkdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocSectionMarkdownComponent);
    component = fixture.componentInstance;
    component.section = {
      content: '# Test something something',
      type: 'markdown'
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
