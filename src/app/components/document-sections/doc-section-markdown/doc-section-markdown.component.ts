import {Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter} from '@angular/core';
import {SettingsService} from 'src/app/services/settings.service';

@Component({
    selector: 'app-doc-section-markdown',
    templateUrl: './doc-section-markdown.component.html',
    styleUrls: ['./doc-section-markdown.component.scss']
})
export class DocSectionMarkdownComponent implements OnInit {

    @Input()
    readOnly = false;

    @Input()
    section: {
        type: string,
        content: string
    };

    @Output()
    changeEvent = new EventEmitter<any>();

    mde: any;

    @ViewChild('textarea') textareaElement: ElementRef;

    constructor(
        private settings: SettingsService
    ) {
    }

    ngOnInit() {

        if (!this.readOnly) {
            setTimeout(() => {
                this.mde = new window['EasyMDE']({
                    element: this.textareaElement.nativeElement,
                    toolbar: false,
                    status: false,
                    forceSync: true,
                    spellChecker: this.settings.values.editor_spellchecker
                });

                this.mde.codemirror.on('change', () => {
                    this.sendChangeEvent(this.mde.value());
                });

                console.log(this.mde);
            }, 1);
        }
    }

    sendChangeEvent(value) {
        this.section.content = value;
        this.changeEvent.emit(this.section);
    }

}
