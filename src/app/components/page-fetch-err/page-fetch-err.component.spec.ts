import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFetchErrComponent } from './page-fetch-err.component';

describe('PageFetchErrComponent', () => {
  let component: PageFetchErrComponent;
  let fixture: ComponentFixture<PageFetchErrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFetchErrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFetchErrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
