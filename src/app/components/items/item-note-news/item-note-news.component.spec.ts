import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemNoteNewsComponent } from './item-note-news.component';

describe('ItemNoteNewsComponent', () => {
  let component: ItemNoteNewsComponent;
  let fixture: ComponentFixture<ItemNoteNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemNoteNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemNoteNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
