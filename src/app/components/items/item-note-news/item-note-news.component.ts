import {AfterContentInit, Component, Input, OnInit} from '@angular/core';
import {NoteThumb} from '../../../types/noteThumb';

@Component({
    selector: 'app-item-note-news',
    templateUrl: './item-note-news.component.html',
    styleUrls: ['./item-note-news.component.scss']
})
export class ItemNoteNewsComponent implements AfterContentInit {
    @Input()
    note: NoteThumb;

    creationDate: string;
    lastModifiedDeltaString: string;

    constructor() {

    }

    ngAfterContentInit(): void {
        this.creationDate = new Date(this.note.created_at).toLocaleDateString();
        this.lastModifiedDeltaString = this.calcDateDeltaString(this.note.modified_at);
    }

    calcDateDeltaString(time) {
        time = new Date().getTime() - time; // Get delta

        const minutes = time / 60 / 1000;
        const hours = minutes / 60;
        const days = hours / 24;

        let final;
        if (Math.floor(minutes) > 1) {
            final = Math.floor(minutes) + ' minutes ago';
        }
        if (Math.floor(minutes) === 1) {
            final = Math.floor(minutes) + ' minute ago';
        }

        if (Math.floor(hours) > 1) {
            final = Math.floor(hours) + ' hours ago';
        }
        if (Math.floor(hours) === 1) {
            final = Math.floor(hours) + ' hour ago';
        }

        if (Math.floor(days) > 1) {
            final = Math.floor(days) + ' days ago';
        }
        if (Math.floor(days) === 1) {
            final = Math.floor(days) + ' day ago';
        }

        if (!final) {
            final = 'a few seconds ago';
        }

        return final;
    }
}
