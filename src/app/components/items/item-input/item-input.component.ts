import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-item-input',
  templateUrl: './item-input.component.html',
  styleUrls: ['./item-input.component.scss']
})
export class ItemInputComponent implements OnInit {
  @Input() value: string;
  @Input() data: {
    type?: 'text'
    placeholder?: string,
    invalid?: boolean,
    focus?: boolean
  };

  @Output() valueChange = new EventEmitter<string>();
  @Output() submit = new EventEmitter<string>();

  @ViewChild('input') inputElement: ElementRef;

  throttle = false; // To prevent additional submit when the enter key is held

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      if (!this.inputElement || !this.data.focus) { return; }

      this.inputElement.nativeElement.focus();
    }, 1);
  }

  emitChange() {
    this.valueChange.emit(this.value);
  }

  emitSubmit() {
    if (this.throttle) { return; }
    this.throttle = true;
    this.submit.emit(this.value);
  }

  resetThrottle() {
    this.throttle = false;
  }

}
