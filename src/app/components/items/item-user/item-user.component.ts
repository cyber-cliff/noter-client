import {Component, Input, OnInit} from '@angular/core';
import {UserProfile} from '../../../types/userProfile';
import {OverlayService} from '../../../shell/overlays/overlay.service';

@Component({
  selector: 'app-item-user',
  templateUrl: './item-user.component.html',
  styleUrls: ['./item-user.component.scss']
})
export class ItemUserComponent implements OnInit {

  @Input()
  user: UserProfile;

  constructor(
      private overlay: OverlayService
  ) { }

  ngOnInit(): void {
  }

  openUserOverlay() {
    this.overlay.showOverlay('user', {userId: this.user.id});
  }

}
