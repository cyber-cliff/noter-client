import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
    @Input() data: {
        icon?: string,

        label: string,
        endLabel?: string,

        disabled?: boolean,
        active?: boolean,
        expandable?: boolean,
        externalLink?: boolean,

        dropdownHead: boolean,
        dropdownExpanded: boolean,

        loading: boolean,

        style?: string,
        background?: string,

        notInteractive?: boolean

        margin: string
    };

    constructor() {
    }

    ngOnInit() {
    }

}
