import { Component, OnInit, Input } from '@angular/core';
import { EventEmitterService } from 'src/app/event-emitter.service';

@Component({
  selector: 'app-list-item-user',
  templateUrl: './list-item-user.component.html',
  styleUrls: ['./list-item-user.component.scss']
})
export class ListItemUserComponent implements OnInit {
  @Input() user: any;

  baseAddress = location.origin;

  constructor(private eventEmitter: EventEmitterService) { }

  ngOnInit() {
  }

  // thx https://stackoverflow.com/questions/49102724/angular-5-copy-to-clipboard
  copyText(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.eventEmitter.pushNotif({
      icon: 'fas fa-copy',
      details: 'Link copied!',
      color: 'var(--color-green)',
      duration: 1500
    });
  }

}
