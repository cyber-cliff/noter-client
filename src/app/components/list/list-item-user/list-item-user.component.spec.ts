import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemUserComponent } from './list-item-user.component';

describe('ListItemUserComponent', () => {
  let component: ListItemUserComponent;
  let fixture: ComponentFixture<ListItemUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListItemUserComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemUserComponent);
    component = fixture.componentInstance;
    component.user = {
      active_at: 0,
      badges: ['dev', 'verified'],
      created_at: 1576164788708,
      description: 'Developer & admin of Noter (this thing), and few other things.',
      id: '5df25db44fd1f104b04a70d4',
      username: 'fajsiex',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
