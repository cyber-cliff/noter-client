import {Component, Input, OnInit} from '@angular/core';
import {ApiService} from '../../../api.service';

@Component({
    selector: 'app-tag-dropdown',
    templateUrl: './tag-dropdown.component.html',
    styleUrls: ['./tag-dropdown.component.scss']
})
export class TagDropdownComponent implements OnInit {

    @Input()
    userId: string;

    @Input()
    tag: { id: string, name: string, color: string };

    expanded = false;

    fetching = false;
    res: any;

    constructor(
        private api: ApiService
    ) {
    }

    ngOnInit(): void {
    }

    toggleExpansion() {
        this.expanded = !this.expanded;

        if (this.expanded && !this.fetching) { // If we just expanded the thing, we need to fetch the notes
            this.fetchNotes();
        }
    }

    async fetchNotes() {
        this.fetching = true;
        this.res = await this.api.request('users/notes/get', {
            user_id: this.userId,
            tag_ids: [this.tag.id]
        }, true);
        this.fetching = false;
    }

}
