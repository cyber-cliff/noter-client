import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemNoteComponent } from './list-item-note.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('ListItemNoteComponent', () => {
  let component: ListItemNoteComponent;
  let fixture: ComponentFixture<ListItemNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [ListItemNoteComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemNoteComponent);
    component = fixture.componentInstance;
    component.note = {
      access: 0,
      author: '5df25db44fd1f104b04a70d4',
      created_at: 1584750212876,
      encrypted: false,
      id: '5e755e84ee525f0d17ca04da',
      modified_at: 1585928261982,
      tags: ['5e5ebeb18be535008525424f'],
      title: 'a new note for testing',
      type: 'doc'
    };
    component.availableTags = [
      {id: '5e5ebeb18be535008525424f', author: '5df25db44fd1f104b04a70d4', name: 'Test tag', color: '#b3ff00'}
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
