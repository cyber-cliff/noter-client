import {Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, ViewChild, ElementRef} from '@angular/core';
import {EventEmitterService} from 'src/app/event-emitter.service';
import {OverlayService} from 'src/app/shell/overlays/overlay.service';
import {NoteStorageService} from 'src/app/services/note-storage.service';
import {ApiService} from 'src/app/api.service';
import {SettingsService} from 'src/app/services/settings.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-list-item-note',
    templateUrl: './list-item-note.component.html',
    styleUrls: ['./list-item-note.component.scss']
})
export class ListItemNoteComponent implements OnInit {
    @Input() note: any;
    @Input() availableTags?: any;
    @Input() compact: boolean;

    @Output('noteUpdate')
    noteUpdateEvent = new EventEmitter<any>();

    @ViewChild('item') itemElement: ElementRef;
    @ViewChild('floatbar') floatBarElement: ElementRef;

    baseAddress = location.origin;

    noteTags = [];

    modifiedAtString = '';
    modifiedAtStringMobile = '';
    modifiedAtDateString = '';

    constructor(
        private eventEmitter: EventEmitterService,
        private overlayService: OverlayService,
        private router: Router,
        private ref: ChangeDetectorRef,
        public noteStore: NoteStorageService,
        public api: ApiService,
        public settings: SettingsService
    ) {
    }

    ngOnInit() {
        if (!this.note.tags || !this.availableTags) {
            this.noteTags = [];
        } // Prevent errors if note.tags is false and therefore not iterable
        for (const tagId of this.note.tags) {
            this.noteTags.push(this.availableTags.filter(aTag => aTag.id === tagId)[0]);
        }

        console.log(this.note);

        this.modifiedAtString = this.calcDateDeltaString(this.note.modified_at);
        this.modifiedAtStringMobile = this.calcDateDeltaStringMobile(this.note.modified_at);
        this.modifiedAtDateString = new Date(this.note.modified_at).toLocaleString();

        setTimeout(() => {
            this.ref.detach();
        }, 0);
    }

    async deleteNote() {
        const deleteSuccess = await this.overlayService.showOverlay('delete-confirm', this.note);

        if (deleteSuccess) {
            this.noteUpdateEvent.emit();
        }
    }

    keyControl(event) {
        if (event.key === 'Enter') {
            this.router.navigateByUrl('/n/' + this.note.id);
        }
    }

    openProps() {
        if (this.note.author !== this.api.session.user_id) {
            return;
        }
        this.openPropsAsync();
        return false;
    }

    async openPropsAsync() {
        const returnValue = await this.overlayService.showOverlay('prop-edit', JSON.parse(JSON.stringify(this.note)));

        if (returnValue) {
            this.noteUpdateEvent.emit();
        }
    }

    adjustFloatBarPosition() {
        this.floatBarElement.nativeElement.style.top = this.itemElement.nativeElement.getBoundingClientRect().top + 'px';
        this.floatBarElement.nativeElement.style.left =
            this.itemElement.nativeElement.getBoundingClientRect().left +
            this.itemElement.nativeElement.getBoundingClientRect().width +
            'px';
    }

    // thx https://stackoverflow.com/questions/49102724/angular-5-copy-to-clipboard
    copyText(val: string) {
        const selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = val;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);
        this.eventEmitter.pushNotif({
            icon: 'fas fa-copy',
            details: 'Link copied!',
            color: 'var(--color-green)',
            duration: 1500
        });
    }

    calcDateDeltaString(time) {
        time = new Date().getTime() - time; // Get delta

        const minutes = time / 60 / 1000;
        const hours = minutes / 60;
        const days = hours / 24;

        let final;
        if (Math.floor(minutes) > 1) {
            final = Math.floor(minutes) + ' minutes ago';
        }
        if (Math.floor(minutes) === 1) {
            final = Math.floor(minutes) + ' minute ago';
        }

        if (Math.floor(hours) > 1) {
            final = Math.floor(hours) + ' hours ago';
        }
        if (Math.floor(hours) === 1) {
            final = Math.floor(hours) + ' hour ago';
        }

        if (Math.floor(days) > 1) {
            final = Math.floor(days) + ' days ago';
        }
        if (Math.floor(days) === 1) {
            final = Math.floor(days) + ' day ago';
        }

        if (!final) {
            final = 'a few seconds ago';
        }

        return final;
    }

    calcDateDeltaStringMobile(time) {
        time = new Date().getTime() - time; // Get delta

        const minutes = time / 60 / 1000;
        const hours = minutes / 60;
        const days = hours / 24;

        let final;
        if (Math.floor(minutes) >= 1) {
            final = Math.floor(minutes) + 'm';
        }

        if (Math.floor(hours) >= 1) {
            final = Math.floor(hours) + 'h';
        }

        if (Math.floor(days) >= 1) {
            final = Math.floor(days) + 'd';
        }

        if (!final) {
            final = '~s';
        }

        return final;
    }
}
