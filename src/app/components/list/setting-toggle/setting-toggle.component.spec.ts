import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingToggleComponent } from './setting-toggle.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('SettingToggleComponent', () => {
  let component: SettingToggleComponent;
  let fixture: ComponentFixture<SettingToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [ SettingToggleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingToggleComponent);
    component = fixture.componentInstance;
    component.data = {
      main: {
        name: 'Toggle test (appearance_blurs)',
        icon: 'fas fa-bug'
      },
      setting: 'appearance_blurs'
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
