import { Component, OnInit, Input } from '@angular/core';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-setting-toggle',
  templateUrl: './setting-toggle.component.html',
  styleUrls: ['./setting-toggle.component.scss']
})
export class SettingToggleComponent implements OnInit {
  @Input() data: { main: any, setting: string };

  currentValue = false;

  constructor(
    private settings: SettingsService
  ) { }

  ngOnInit() {
    this.currentValue = this.settings.values[this.data.setting];
  }

  toggleSetting() {
    this.currentValue = !this.currentValue;
    this.settings.values[this.data.setting] = this.currentValue;
    this.settings.saveSettings();
  }
}
