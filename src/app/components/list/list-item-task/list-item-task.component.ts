import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-list-item-task',
  templateUrl: './list-item-task.component.html',
  styleUrls: ['./list-item-task.component.scss']
})
export class ListItemTaskComponent implements OnInit {
  @Input() task: any;
  @Input() availableTags: any;

  @Output()
  reloadList = new EventEmitter<any>();

  taskTags = [];

  overdue = false;

  fetchingUpdate = false;

  constructor(
    public api: ApiService
  ) { }

  ngOnInit(): void {
    const groupStartTimestamp = new Date(
      new Date(this.task.timestamp).toLocaleDateString()
    ).getTime();

    const todayStartTimestamp = new Date(
      new Date().toLocaleDateString()
    ).getTime();

    if (groupStartTimestamp < todayStartTimestamp) {
      this.overdue = true;
    }

    if (!this.task.tags) { this.taskTags = []; return; } // Prevent errors if task.tags is false and therefore not iterable
    for (const tagId of this.task.tags) {
      this.taskTags.push(this.availableTags.filter(aTag => aTag.id === tagId)[0]);
    }
  }

  async toggleCompletedStatus() {
    if (this.fetchingUpdate) {
      return;
    }

    this.fetchingUpdate = true;

    const prevState = this.task.completed;
    this.task.completed = !this.task.completed;

    const res = await this.api.request('tasks/update', {
      task_id: this.task.id,
      task: {
        completed: this.task.completed
      }
    }, true);

    this.fetchingUpdate = false;

    if (!res.success) {
      this.task.completed = prevState;
      return;
    }
  }

  async deleteSelf() {
    if (this.fetchingUpdate) {
      return;
    }

    this.fetchingUpdate = true;

    const res = await this.api.request('tasks/delete', {
      task_id: this.task.id
    }, true);

    this.fetchingUpdate = false;

    if (!res.success) {
      return;
    }

    this.reloadList.emit();
  }

}
