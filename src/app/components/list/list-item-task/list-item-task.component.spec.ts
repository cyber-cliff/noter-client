import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemTaskComponent } from './list-item-task.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('ListItemTaskComponent', () => {
  let component: ListItemTaskComponent;
  let fixture: ComponentFixture<ListItemTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [ListItemTaskComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemTaskComponent);
    component = fixture.componentInstance;
    component.task = {
      author: '5df25db44fd1f104b04a70d4',
      completed: true,
      id: '5e9b0964e14d7c007cc9245f',
      name: 'Something something task',
      tags: [],
      timestamp: 1587427200000
    };
    component.availableTags = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
