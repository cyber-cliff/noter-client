import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingDropdownComponent } from './setting-dropdown.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('SettingDropdownComponent', () => {
  let component: SettingDropdownComponent;
  let fixture: ComponentFixture<SettingDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [SettingDropdownComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingDropdownComponent);
    component = fixture.componentInstance;
    component.data = {
      main: {
        name: 'Test setting (appearance_theme)',
        icon: 'fas fa-bug'
      },
      setting: 'appearance_theme',
      items: [
        { name: 'Dark', value: 'dark' },
        { name: 'Light (not finished)', value: 'light' }
      ]
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
