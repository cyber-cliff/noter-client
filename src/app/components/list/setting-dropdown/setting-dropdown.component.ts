import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-setting-dropdown',
  templateUrl: './setting-dropdown.component.html',
  styleUrls: ['./setting-dropdown.component.scss']
})
export class SettingDropdownComponent implements OnInit {
  @Input() data: { main: any, setting: string, items: Array<any> };

  setting: any;
  expanded = false;
  currentValueName = "";

  constructor(public settings: SettingsService) {
  }

  ngOnInit() {
    this.getCurrentValueName();
  }

  change(value: any) {
    this.settings.values[this.data.setting] = value;
    this.settings.saveSettings();
    this.getCurrentValueName();
    this.expanded = false;
  }

  getCurrentValueName() {
    const currentItem = this.data.items.filter((item) => {
      return this.settings.values[this.data.setting] === item.value;
    })[0];

    this.currentValueName = currentItem.name;
  }
}
