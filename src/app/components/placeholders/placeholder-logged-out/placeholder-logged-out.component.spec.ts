import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceholderLoggedOutComponent } from './placeholder-logged-out.component';

describe('PlaceholderLoggedOutComponent', () => {
  let component: PlaceholderLoggedOutComponent;
  let fixture: ComponentFixture<PlaceholderLoggedOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceholderLoggedOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceholderLoggedOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
