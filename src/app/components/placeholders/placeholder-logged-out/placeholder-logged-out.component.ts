import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-placeholder-logged-out',
  templateUrl: './placeholder-logged-out.component.html',
  styleUrls: ['./placeholder-logged-out.component.scss']
})
export class PlaceholderLoggedOutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
