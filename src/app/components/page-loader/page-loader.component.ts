import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-loader',
  templateUrl: './page-loader.component.html',
  styleUrls: ['./page-loader.component.scss']
})
export class PageLoaderComponent implements OnInit {

  msgs = [ // https://www.factretriever.com/cat-facts
    'A group of cats is called a "clowder."',
    'Cats make about 100 different sounds.',
    'A cat can jump up to five times its own height in a single bound.',
    'A cat usually has about 12 whiskers on each side of its face.',
    'A cat\'s jaw can’t move sideways.',
    'A cat\'s nose pad is ridged with a unique pattern, just like the fingerprint of a human.',
    'Cats spend nearly 1/3 of their waking hours cleaning themselves.'
  ];

  msg = '';

  constructor() { }

  ngOnInit() {
    const sMsgs = this.msgs;
    this.msg = sMsgs[Math.floor(Math.random() * sMsgs.length)];
  }

}
