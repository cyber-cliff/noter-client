import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageUnauthedComponent } from './page-unauthed.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('PageUnauthedComponent', () => {
  let component: PageUnauthedComponent;
  let fixture: ComponentFixture<PageUnauthedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [ PageUnauthedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageUnauthedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
