import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-page-unauthed',
  templateUrl: './page-unauthed.component.html',
  styleUrls: ['./page-unauthed.component.scss']
})
export class PageUnauthedComponent implements OnInit {

  constructor(public api: ApiService) { }

  ngOnInit() {
  }

}
