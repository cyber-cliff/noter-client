export interface Theme {
    name: string;
    properties: any;
}

export const light: Theme = {
    name: 'light',
    properties: {
        '--color-background': '#fff',
        '--color-background-l1': '#d8d8d8',
        '--color-background-l2': '#ccc',
        '--color-background-l3': '#a8a8a8',

        '--color-gray': '#888',

        '--color-text': '#000',
        '--color-text-inverted': '#fff',

        '--color-red': '#d00',
        '--color-orange': '#d80',
        '--color-yellow': '#cc0',
        '--color-lime': '#8d1',
        '--color-green': '#0d0',
        '--color-cyan': '#0dd',
        '--color-lblue': '#18f',
        '--color-blue': '#02f',
        '--color-purple': '#42f',
        '--color-pink': '#f0f',

        '--color-background-app': 'rgba(255,255,255,0.7)',

        '--gradient-overlay-1': '#fff',
        '--gradient-overlay-2': 'rgba(255,255,255,0.5)',
        '--gradient-overlay-f': 'rgba(255,255,255,0.9)',

        '--filter-hover': 'brightness(0.7)',
        '--filter-hover-inverted': 'brightness(1.25)'
    }
};

export const dark: Theme = {
    name: 'dark',
    properties: {
        '--color-background': '#000',
        '--color-background-l1': 'rgba(17, 17, 17, 1)',
        '--color-background-l2': 'rgba(24, 24, 24, 1)',
        '--color-background-l3': '#222',

        '--color-gray': '#888',

        '--color-text': '#fff',
        '--color-text-inverted': '#000',

        '--color-red': '#f55',
        '--color-orange': '#fa0',
        '--color-yellow': '#ff5',
        '--color-lime': '#af5',
        '--color-green': '#5f5',
        '--color-cyan': '#5ff',
        '--color-lblue': '#5af',
        '--color-blue': '#05f',
        '--color-purple': '#55f',
        '--color-pink': '#f5f',

        '--color-background-app': 'rgba(0,0,0,0.7)',

        '--gradient-overlay-1': '#000',
        '--gradient-overlay-2': 'rgba(0,0,0,0.5)',
        '--gradient-overlay-f': 'rgba(0,0,0,0.9)',


        '--filter-hover': 'brightness(1.25)',
        '--filter-hover-inverted': 'brightness(0.7)'
    }
};
