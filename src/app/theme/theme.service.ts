import { Injectable } from '@angular/core';
import { Theme, light, dark } from './theme';
import { EventEmitterService } from '../event-emitter.service';

// thx https://www.freecodecamp.org/news/how-to-create-themes-for-your-angular-7-apps-using-css-variables-69251690e9c5/

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private active: Theme = light;
  private availableThemes: Theme[] = [light, dark];

  private bgMode = 'solid';

  private rainbowModeColors = {
    r: 255,
    g: 0,
    b: 0,
    stage: 0
  };

  constructor(private eventEmitter: EventEmitterService) { }

  getAvailableThemes(): Theme[] {
    return this.availableThemes;
  }

  getActiveTheme(): Theme {
    return this.active;
  }

  setDarkTheme(): void {
    this.setActiveTheme(dark);
  }

  setLightTheme(): void {
    this.setActiveTheme(light);
  }

  rainbowCycle() {
    const step = 7;
    switch (this.rainbowModeColors.stage) {
      case 0:
        this.rainbowModeColors.g += step;
        break;
      case 1:
        this.rainbowModeColors.r -= step;
        break;
      case 2:
        this.rainbowModeColors.b += step;
        break;
      case 3:
        this.rainbowModeColors.g -= step;
        break;
      case 4:
        this.rainbowModeColors.r += step;
        break;
      case 5:
        this.rainbowModeColors.b -= step;
        break;
    }

    // * Go to stage 1
    if (this.rainbowModeColors.g > 255) {
      this.rainbowModeColors.g = 255;
      this.rainbowModeColors.stage++;
    }

    // * Go to stage 2
    if (this.rainbowModeColors.r < 0) {
      this.rainbowModeColors.r = 0;
      this.rainbowModeColors.stage++;
    }

    // * Go to stage 3
    if (this.rainbowModeColors.b > 255) {
      this.rainbowModeColors.b = 255;
      this.rainbowModeColors.stage++;
    }

    // * Go to stage 4
    if (this.rainbowModeColors.g < 0) {
      this.rainbowModeColors.g = 0;
      this.rainbowModeColors.stage++;
    }

    // * Go to stage 5
    if (this.rainbowModeColors.r > 255) {
      this.rainbowModeColors.r = 255;
      this.rainbowModeColors.stage++;
    }

    // * Go to stage 0 again
    if (this.rainbowModeColors.b < 0) {
      this.rainbowModeColors.b = 0;
      this.rainbowModeColors.stage = 0;
    }

    document.documentElement.style.setProperty(
      '--color-accent',
      `rgb(${this.rainbowModeColors.r}, ${this.rainbowModeColors.g}, ${this.rainbowModeColors.b})`
    );

    setTimeout(() => {
      this.rainbowCycle();
    }, 200);
  }

  setTransitionDuration(duration) {
    document.documentElement.style.setProperty(
      '--transition',
      `${duration}s ease`
    );
  }

  setAccentColor(color) {
    document.documentElement.style.setProperty(
      '--color-accent',
      color
    );
  }

  setBlurFilter(enabled: boolean) {
    document.documentElement.style.setProperty(
      '--filter-blur',
      (enabled) ? 'blur(3px)' : 'none'
    );
  }

  contrastItemComponents() {
    document.documentElement.style.setProperty(
      '--item-background',
      'var(--color-lblue)'
    );
  }

  async setThemeFromString(themeName: string, bg: string) {
    switch (themeName) {
      case 'light':
        this.setActiveTheme(light);
        break;
      default:
        this.setActiveTheme(dark);
    }

    if (bg !== this.bgMode) {
      this.bgMode = bg;

      switch (bg) {
        case 'apod':
          const d = new Date();
          const todayDate = `${d.getUTCFullYear()}-${d.getUTCMonth().toString() + 1}-${d.getUTCDate().toString()}`;

          if (localStorage.n_apod_date === todayDate) {
            if (!localStorage.n_apod_url) { return; }

            document.querySelector('body').style.backgroundImage = `url(${localStorage.n_apod_url})`;
            document.querySelector('body').style.backgroundSize = `cover`;

            return;
          }

          const rawRes = await fetch('https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY');
          const res = await rawRes.json();

          if (res.media_type !== 'image') { return; }

          if (res.copyright) {
            this.eventEmitter.pushNotif({
              icon: 'fas fa-copyright',
              details: `Background image has the following copyright information: ${JSON.stringify(res.copyright)}`,
              color: 'var(--color-lblue)',
              duration: 0
            });
          }

          localStorage.n_apod_date = todayDate;
          localStorage.n_apod_url = res.url;

          document.querySelector('body').style.background = `url(${res.url})`;
          document.querySelector('body').style.backgroundSize = `cover`;

          break;
        case 'accent':
          document.querySelector('body').style.background = 'var(--color-accent)';
          break;
        default:
          document.querySelector('body').style.background = 'var(--color-background)';
      }
    }
  }

  setActiveTheme(theme: Theme): void {
    this.active = theme;

    localStorage.setItem('theme', theme.name);

    Object.keys(this.active.properties).forEach(property => {
      document.documentElement.style.setProperty(
        property,
        this.active.properties[property]
      );
    });
  }
}
