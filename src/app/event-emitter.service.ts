import { Injectable, EventEmitter } from '@angular/core';
import { Subscription, SubscriptionLike } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {
  notifEmitter = new EventEmitter();
  sidebarEmitter = new EventEmitter();
  noteEditEmitter = new EventEmitter();

  notifSub: Subscription;
  sidebarSub: Subscription;
  noteEditSub: Subscription;

  loginStatusChangeEmitter = new EventEmitter();

  constructor() { }

  pushNotif(notif) {
    this.notifEmitter.emit({eventType: 'push', notif});
  }
  dismissAllNotifs() {
    this.notifEmitter.emit({eventType: 'dismissAll'});
  }
  onLogIn() {
    this.sidebarEmitter.emit({eventType: 'login'});
  }
  onLogOut() {
    this.sidebarEmitter.emit({eventType: 'logout'});
  }
  saveNote() {
    this.noteEditEmitter.emit({eventType: 'save'});
  }
}
