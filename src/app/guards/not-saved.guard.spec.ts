import { TestBed, async, inject } from '@angular/core/testing';

import { NotSavedGuard } from './not-saved.guard';

describe('NotSavedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotSavedGuard]
    });
  });

  it('should ...', inject([NotSavedGuard], (guard: NotSavedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
