export interface UserProfile {
    id: string;
    username: string;
    description: string;
    badges: Array<string>;

    created_at: number;
    active_at: number;
}
