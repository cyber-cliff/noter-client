export interface NoteThumb {
    id: string;
    title: string;
    author: string;

    type: string;

    created_at: number;
    modified_at: number;

    access: number;
    encrypted?: boolean;
    news_post?: boolean;

    tags: Array<number>;
}
