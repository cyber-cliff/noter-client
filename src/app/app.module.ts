import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from '../environments/environment';
import { NgxEchartsModule } from 'ngx-echarts';

// * Plugin modules
import { MarkdownModule, MarkedOptions } from 'ngx-markdown';
import { TooltipModule } from 'ng2-tooltip-directive';
import { ChartsModule } from 'ng2-charts';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

// * App base modules
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// * ==================== PAGES

// * Note listing pages
import { UserNotesComponent } from './pages/user/user-notes/user-notes.component';
import { FilterBarComponent } from './pages/user/user-notes/filter-bar/filter-bar.component';


// * Browse pages
import { BrowseComponent } from './pages/browse/browse.component';

// * Downloaded & Saved notes pages
import { DownloadedNotesComponent } from './pages/saved-notes/downloaded-notes/downloaded-notes.component';
import { BookmarkedNotesComponent } from './pages/saved-notes/bookmarked-notes/bookmarked-notes.component';

// * Stats pages
import { StatsComponent } from './pages/info/stats/stats.component';

// * Error pages
import { NotFoundComponent } from './pages/errors/not-found/not-found.component';

// * Debug tests
import { TestThemeComponent } from './pages/tests/test-theme/test-theme.component';

// * Full page insert components (loaders, errors, and so on)
import { PageLoaderComponent } from './components/page-loader/page-loader.component';
import { PageFetchErrComponent } from './components/page-fetch-err/page-fetch-err.component';
import { PageUnauthedComponent } from './components/page-unauthed/page-unauthed.component';

// * ==================== SHELL
// * Sidebar
import { SidebarComponent } from './shell/sidebar/sidebar.component';

// * Notifs
import { NotifsComponent } from './shell/notifs/notifs.component';

// * Splash screen
import { SplashScreenComponent } from './shell/splash-screen/splash-screen.component';
import { SplashLogoComponent } from './shell/splash-screen/splash-logo/splash-logo.component';


// * Overlays
import { OverlaysComponent } from './shell/overlays/overlays.component';
import { OverlayAccountComponent } from './shell/overlays/overlay-account/overlay-account.component';
import { OverlayIndexComponent } from './shell/overlays/overlay-index/overlay-index.component';
import { OverlayDeleteConfirmComponent } from './shell/overlays/overlay-delete-confirm/overlay-delete-confirm.component';
import { OverlayPropEditComponent } from './shell/overlays/overlay-prop-edit/overlay-prop-edit.component';
import { OverlayDebugComponent } from './shell/overlays/overlay-debug/overlay-debug.component';
import { OverlayLoginComponent } from './shell/overlays/overlay-login/overlay-login.component';
import { OverlayNewNoteComponent } from './shell/overlays/overlay-new-note/overlay-new-note.component';
import { OverlaySettingsComponent } from './shell/overlays/overlay-settings/overlay-settings.component';
import { OverlayMdGuideComponent } from './shell/overlays/overlay-md-guide/overlay-md-guide.component';
import { OverlayNoteSaveComponent } from './shell/overlays/overlay-note-save/overlay-note-save.component';
import { OverlayListGroupingComponent } from './shell/overlays/overlay-list-grouping/overlay-list-grouping.component';

// * Login overlay sections
import { LoginSectionLoginComponent } from './shell/overlays/overlay-login/login-section-login/login-section-login.component';
import { LoginSectionSignupComponent } from './shell/overlays/overlay-login/login-section-signup/login-section-signup.component';
// tslint:disable-next-line:max-line-length
import { LoginSectionAutologinfailComponent } from './shell/overlays/overlay-login/login-section-autologinfail/login-section-autologinfail.component';

// * GENERAL COMPONENTS
import { ItemComponent } from './components/items/item/item.component';
import { ListItemNoteComponent } from './components/list/list-item-note/list-item-note.component';
import { ListItemUserComponent } from './components/list/list-item-user/list-item-user.component';
import { SettingDropdownComponent } from './components/list/setting-dropdown/setting-dropdown.component';
import { SettingToggleComponent } from './components/list/setting-toggle/setting-toggle.component';

// * Placeholders
import { PlaceholderLoggedOutComponent } from './components/placeholders/placeholder-logged-out/placeholder-logged-out.component';

// * GUARDS
import { NotSavedGuard } from './guards/not-saved.guard';
import { NoteViewerComponent } from './pages/note-viewer/note-viewer.component';
import { NoteEditorComponent } from './pages/note-editor/note-editor.component';
import { DocSectionMarkdownComponent } from './components/document-sections/doc-section-markdown/doc-section-markdown.component';
import { DocSectionDrawComponent } from './components/document-sections/doc-section-draw/doc-section-draw.component';
// tslint:disable-next-line:max-line-length
import { OverlayEncryptPasswordPromptComponent } from './shell/overlays/overlay-encrypt-password-prompt/overlay-encrypt-password-prompt.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import { ListItemTaskComponent } from './components/list/list-item-task/list-item-task.component';
import { ItemInputComponent } from './components/items/item-input/item-input.component';
import { OverlaySearchComponent } from './shell/overlays/overlay-search/overlay-search.component';
import { OverlayTasksComponent } from './shell/overlays/overlay-tasks/overlay-tasks.component';
import { PlaceholderLoaderComponent } from './components/placeholders/placeholder-loader/placeholder-loader.component';
import { OverlayOpenComponent } from './shell/overlays/overlay-open/overlay-open.component';
import { TagDropdownComponent } from './components/list/tag-dropdown/tag-dropdown.component';
import { ListDividerComponent } from './components/list/list-divider/list-divider.component';
import { ItemNoteNewsComponent } from './components/items/item-note-news/item-note-news.component';
import { ItemUserComponent } from './components/items/item-user/item-user.component';
import { UserBadgesComponent } from './components/common/props/user-badges/user-badges.component';
import { OverlaySectionUserNotesComponent } from './shell/overlays/sections/overlay-section-user-notes/overlay-section-user-notes.component';
import { OverlayUserComponent } from './shell/overlays/overlay-user/overlay-user.component';

@NgModule({
  declarations: [
    AppComponent,
    // * ==================== PAGES
    UserNotesComponent,

    BrowseComponent,

    NoteViewerComponent,
    NoteEditorComponent,

    DownloadedNotesComponent,
    BookmarkedNotesComponent,

    TasksComponent,

    StatsComponent,

    NotFoundComponent,

    TestThemeComponent,

    // * Full page inserts
    PageLoaderComponent,
    PageFetchErrComponent,
    PageUnauthedComponent,

    // * ==================== SHELL
    SidebarComponent,
    NotifsComponent,

    // * Splash screen
    SplashScreenComponent,
    SplashLogoComponent,

    // * Overlays
    OverlayIndexComponent,
    OverlayDeleteConfirmComponent,
    OverlaysComponent,
    OverlayPropEditComponent,
    OverlayAccountComponent,
    OverlayDebugComponent,
    OverlayLoginComponent,
    OverlayNewNoteComponent,
    OverlaySettingsComponent,
    OverlayMdGuideComponent,
    OverlayNoteSaveComponent,
    OverlayListGroupingComponent,
    OverlayEncryptPasswordPromptComponent,

    // * Login overlay sections
    LoginSectionLoginComponent,
    LoginSectionSignupComponent,
    LoginSectionAutologinfailComponent,

    // * ==================== GENERAL COMPONENTS
    ItemComponent,
    ItemInputComponent,

    ListItemNoteComponent,
    ListItemUserComponent,
    ListItemTaskComponent,

    SettingDropdownComponent,
    SettingToggleComponent,

    FilterBarComponent,

    // * Doc sections
    DocSectionMarkdownComponent,
    DocSectionDrawComponent,

    // * Placeholders
    PlaceholderLoggedOutComponent,

    OverlaySearchComponent,

    OverlayTasksComponent,

    PlaceholderLoaderComponent,

    OverlayOpenComponent,

    TagDropdownComponent,

    ListDividerComponent,

    ItemNoteNewsComponent,

    ItemUserComponent,

    UserBadgesComponent,

    OverlaySectionUserNotesComponent,

    OverlayUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot([]),
    RouterTestingModule,
    ReactiveFormsModule,
    FormsModule,
    TooltipModule,
    ChartsModule,
    NgxEchartsModule,
    MarkdownModule.forRoot({
      markedOptions: {
        provide: MarkedOptions,
        useValue: {
          gfm: true,
          breaks: true,
        },
      },
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    NotSavedGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
