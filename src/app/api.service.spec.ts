import { TestBed } from '@angular/core/testing';

import { ApiService } from './api.service';

import {} from 'jasmine';

import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService, TranslateStore, TranslateLoader, TranslateCompiler, TranslateParser, MissingTranslationHandler } from '@ngx-translate/core';
import { InjectionToken } from '@angular/core';

describe('ApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      TranslateModule.forRoot()
    ],
    providers: [
      TranslateService
    ]
  }));

  it('should be created', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service).toBeTruthy();
  });

  it('request info/about', async () => {
    const service: ApiService = TestBed.get(ApiService);
    const res = await service.request('info/about', {});
    expect(res.success).toBeTruthy();
    expect(res.data).toBeTruthy();
  });

  it('request tags/get (should fail due to auth error)', async () => {
    const service: ApiService = TestBed.get(ApiService);
    const res = await service.request('tags/get', {}, true);
    expect(res.success).toBeFalsy();
    expect(res.error.authError).toBeTruthy();
  });
});
