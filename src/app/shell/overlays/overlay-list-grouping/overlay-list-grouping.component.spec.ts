import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayListGroupingComponent } from './overlay-list-grouping.component';

describe('OverlayListGroupingComponent', () => {
  let component: OverlayListGroupingComponent;
  let fixture: ComponentFixture<OverlayListGroupingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlayListGroupingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayListGroupingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
