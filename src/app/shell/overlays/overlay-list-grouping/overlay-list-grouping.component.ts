import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-overlay-list-grouping',
  templateUrl: './overlay-list-grouping.component.html',
  styleUrls: ['./overlay-list-grouping.component.scss']
})
export class OverlayListGroupingComponent implements OnInit {

  @Input() groupMode: string;

  @Output('resolved')
  changeEvent = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  changeMode(newMode) {
    this.groupMode = newMode;
    this.changeEvent.emit(this.groupMode);
    
  }

}
