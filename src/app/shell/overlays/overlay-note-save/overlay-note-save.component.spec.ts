import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayNoteSaveComponent } from './overlay-note-save.component';
import { MarkdownModule } from 'ngx-markdown';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('OverlayNoteSaveComponent', () => {
  let component: OverlayNoteSaveComponent;
  let fixture: ComponentFixture<OverlayNoteSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot(),
        MarkdownModule.forRoot()
      ],
      declarations: [OverlayNoteSaveComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayNoteSaveComponent);
    component = fixture.componentInstance;
    component.noteData = { // TODO: have some templates for this
      note: {
        id: '5e0493f703aa92008108b8c4',
        author: '5df25db44fd1f104b04a70d4',
        title: 'Otec Goriot',
        tags: [
          '5df51eed372de607f41ec914'
        ],
        created_at: 1577358327927,
        modified_at: 1584360016802,
        access: 3,
        encrypted: false,
        doc: {
          head: {
            ndfVersion: 4,
            type: 'doc'
          },
          body: {
            sections: [
              {
                type: 'markdown',
                content: '# Some content'
              }
            ]
          }
        }
      },
      tags: [
        {
          id: '5df51eed372de607f41ec914',
          author: '5df25db44fd1f104b04a70d4',
          name: 'SJL',
          color: '#f80',
          public: false
        }
      ],
      users: [
        {
          id: '5df25db44fd1f104b04a70d4',
          username: 'fajsiex',
          created_at: 1576164788708,
          active_at: 0,
          description: 'Developer & admin of Noter (this thing), and few other things.',
          badges: [
            'dev',
            'verified'
          ]
        }
      ]
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
