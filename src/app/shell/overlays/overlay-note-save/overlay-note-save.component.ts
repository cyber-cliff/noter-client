import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { MarkdownService } from 'ngx-markdown';
import { NoteStorageService } from 'src/app/services/note-storage.service';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-overlay-note-save',
  templateUrl: './overlay-note-save.component.html',
  styleUrls: ['./overlay-note-save.component.scss']
})
export class OverlayNoteSaveComponent implements OnInit {
  @Input() noteData: any;

  @Output('resolved')
  resolveEvent = new EventEmitter<any>();

  fetching = false;

  isBookmarked = false;
  isSavedLocally = false;

  constructor(
    private markdownService: MarkdownService,
    public api: ApiService,
    public noteStore: NoteStorageService
  ) {
  }

  ngOnInit() {
    this.isSavedLocally = this.noteStore.hasNoteData(this.noteData.note.id);
    this.fetchIfBookmarked();
  }

  async bookmarkNote() {
    if (this.fetching || !this.api.session.token) { return; }

    if (this.isBookmarked) {
      this.unbookmarkNote();
      return;
    }

    const noteId = this.noteData.note.id;

    this.fetching = true;
    const res = await this.api.request('users/saved/save', { note_id: noteId }, true);
    this.fetching = false;

    if (res.success) {
      this.isBookmarked = true;
    }
  }

  async unbookmarkNote() {
    const noteId = this.noteData.note.id;

    this.fetching = true;
    const res = await this.api.request('users/saved/remove', { note_id: noteId }, true);
    this.fetching = false;

    if (res.success) {
      this.isBookmarked = false;
    }
  }

  saveLocally() {
    if (this.noteStore.hasNoteData(this.noteData.note.id)) {
      this.deleteFromDownloaded(this.noteData.note.id);
      this.isSavedLocally = this.noteStore.hasNoteData(this.noteData.note.id);
      return;
    }

    this.noteStore.storeNote(this.noteData);

    this.isSavedLocally = this.noteStore.hasNoteData(this.noteData.note.id);
  }

  deleteFromDownloaded(noteId: string) {
    this.noteStore.deleteNoteDataFromStorage(noteId);
  }

  exportHTML() {
    const html = this.markdownService.compile(this.noteData.note.doc.content);

    // thx https://stackoverflow.com/a/50376496
    const blob = new Blob([html], { type: 'text/html' });
    const url = window.URL.createObjectURL(blob);

    const fileName = `${this.noteData.note.title}.html`;

    if (navigator.msSaveOrOpenBlob) {
      navigator.msSaveBlob(blob, fileName);
    } else {
      const a = document.createElement('a');
      a.href = url;
      a.download = fileName;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
    window.URL.revokeObjectURL(url);

    /* const domParser = new DOMParser();
    const doc = domParser.parseFromString(html, 'text/html');

    console.log(doc); */
  }

  async fetchIfBookmarked() {
    if (!this.api.session.token) { return; }

    this.fetching = true;

    const res = await this.api.request('users/saved/get', {}, true);

    this.fetching = false;

    if (res.success) {
      const foundNote = res.data.notes.filter(note => {
        return note.id === this.noteData.note.id;
      })[0];

      if (foundNote) {
        this.isBookmarked = true;
      }
    }
  }

  cancel() {
    this.resolveEvent.emit(false);
  }
}
