import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class OverlayService {

  overlayEmitter = new EventEmitter();
  overlaySub: Subscription;

  resolverEmitter = new EventEmitter();
  resolverSub: Subscription;

  overlayShown: any = false;

  constructor() { }

  async showOverlay(name: string, data?: any): Promise<any> {
    this.overlayShown = name;
    this.overlayEmitter.emit({type: 'show', name, data});

    const returnData = await new Promise(resolve => {
      if (this.resolverSub === undefined) {
        this.resolverSub = this.resolverEmitter.subscribe((resolvedData: any) => {
          this.overlayShown = false;
          resolve(resolvedData);
        });
      }
    });

    this.resolverSub.unsubscribe();
    this.resolverSub = undefined;

    return returnData;
  }

  cancelOverlay() {
    this.overlayEmitter.emit({type: 'cancel'});
  }
}
