import {AfterContentInit, Component, Input, OnInit} from '@angular/core';
import {ApiService} from '../../../api.service';

@Component({
    selector: 'app-overlay-user',
    templateUrl: './overlay-user.component.html',
    styleUrls: ['./overlay-user.component.scss']
})
export class OverlayUserComponent implements AfterContentInit {
    @Input() data: { userId: string };

    fetching = false;
    res: any;

    createdAtDelta: string;
    activeAtDelta: string;

    constructor(
        public api: ApiService
    ) {
    }

    ngAfterContentInit(): void {
        this.fetchProfile();
    }

    async fetchProfile() {
        this.fetching = true;
        this.res = await this.api.request('users/profile/get', {user_id: this.data.userId}, true);

        if (this.res.success) {
            this.createdAtDelta = this.calcDateDeltaString(this.res.data.created_at);
            this.activeAtDelta = this.calcDateDeltaString(this.res.data.active_at);
        }

        this.fetching = false;
    }

    calcDateDeltaString(time: number) {
        if (time <= 1576164788708) {
            return '∞ days ago';
        }
        time = new Date().getTime() - time; // Get delta

        const minutes = time / 60 / 1000;
        const hours = minutes / 60;
        const days = hours / 24;

        let final;
        if (Math.floor(minutes) > 1) {
            final = Math.floor(minutes) + ' minutes ago';
        }
        if (Math.floor(minutes) === 1) {
            final = Math.floor(minutes) + ' minute ago';
        }

        if (Math.floor(hours) > 1) {
            final = Math.floor(hours) + ' hours ago';
        }
        if (Math.floor(hours) === 1) {
            final = Math.floor(hours) + ' hour ago';
        }

        if (Math.floor(days) > 1) {
            final = Math.floor(days) + ' days ago';
        }
        if (Math.floor(days) === 1) {
            final = Math.floor(days) + ' day ago';
        }

        if (!final) {
            final = 'a few seconds ago';
        }

        return final;
    }
}
