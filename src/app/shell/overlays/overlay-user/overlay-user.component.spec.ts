import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayUserComponent } from './overlay-user.component';

describe('OverlayUserComponent', () => {
  let component: OverlayUserComponent;
  let fixture: ComponentFixture<OverlayUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlayUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
