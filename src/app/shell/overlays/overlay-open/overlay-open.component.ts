import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../api.service';
import {ActivityService} from '../../../services/activity.service';
import {NoteStorageService} from '../../../services/note-storage.service';

@Component({
    selector: 'app-overlay-open',
    templateUrl: './overlay-open.component.html',
    styleUrls: ['./overlay-open.component.scss']
})
export class OverlayOpenComponent implements OnInit {

    recentlyOpenedNotes = [];

    bnotesFetching = false;
    bnotesRes: {
        success: boolean,
        data?: any,
        error?: any
    };

    downloadedNotes = [];

    constructor(
        private activityService: ActivityService,
        private noteStore: NoteStorageService,
        public api: ApiService
    ) {
    }

    ngOnInit(): void {
        this.getRecentlyOpenedNotes();
        this.getDownloadedNotes();
        if (this.api.session.user_id) {
            this.fetchBnotes();
        }
    }

    getRecentlyOpenedNotes() {
        this.recentlyOpenedNotes = this.activityService.getRecentlyOpenedNotes();
    }

    async fetchBnotes() {
        this.bnotesFetching = true;
        this.bnotesRes = await this.api.request('users/saved/get', {}, true);
        this.bnotesFetching = false;
    }

    getDownloadedNotes() {
        this.downloadedNotes = this.noteStore.storedNotes;
        this.downloadedNotes = this.downloadedNotes.sort((a, b) => {
            return (b.storeLastUpdate - a.storeLastUpdate);
        });
    }

}
