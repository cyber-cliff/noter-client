import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayOpenComponent } from './overlay-open.component';

describe('OverlayOpenComponent', () => {
  let component: OverlayOpenComponent;
  let fixture: ComponentFixture<OverlayOpenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlayOpenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayOpenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
