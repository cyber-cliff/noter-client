import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayIndexComponent } from './overlay-index.component';

describe('OverlayIndexComponent', () => {
  let component: OverlayIndexComponent;
  let fixture: ComponentFixture<OverlayIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OverlayIndexComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayIndexComponent);
    component = fixture.componentInstance;
    component.index = [
      { id: 'test-heading', title: 'Test heading', level: 1, el: {} }
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
