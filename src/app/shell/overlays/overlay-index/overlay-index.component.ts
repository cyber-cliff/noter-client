import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-overlay-index',
    templateUrl: './overlay-index.component.html',
    styleUrls: ['./overlay-index.component.scss']
})
export class OverlayIndexComponent implements OnInit {
    @Input() index: Array<{ id: string, title: string, level: number, el: any }>;

    @Output('resolved')
    resolveEvent = new EventEmitter<any>();

    constructor() {
    }

    ngOnInit() {
    }

    scrollTo(heading: any) {
        // Compatibility with firefox
        if (heading.el.scrollIntoViewIfNeeded) {
            heading.el.scrollIntoViewIfNeeded();
        } else {
            heading.el.scrollIntoView();
        }
        heading.el.classList.add('highlight');
        setTimeout(() => {
            heading.el.classList.remove('highlight');
        }, 2500);
        this.resolveEvent.emit(true);
    }

}
