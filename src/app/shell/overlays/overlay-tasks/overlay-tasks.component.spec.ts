import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayTasksComponent } from './overlay-tasks.component';

describe('OverlayTasksComponent', () => {
  let component: OverlayTasksComponent;
  let fixture: ComponentFixture<OverlayTasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlayTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
