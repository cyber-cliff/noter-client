import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayDeleteConfirmComponent } from './overlay-delete-confirm.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('OverlayDeleteConfirmComponent', () => {
  let component: OverlayDeleteConfirmComponent;
  let fixture: ComponentFixture<OverlayDeleteConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [ OverlayDeleteConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayDeleteConfirmComponent);
    component = fixture.componentInstance;
    component.note = {
      title: 'Note title' // TODO: have some global note thumb reference
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
