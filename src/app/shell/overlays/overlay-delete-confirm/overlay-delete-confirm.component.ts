import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EventEmitterService } from 'src/app/event-emitter.service';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-overlay-delete-confirm',
  templateUrl: './overlay-delete-confirm.component.html',
  styleUrls: ['./overlay-delete-confirm.component.scss']
})
export class OverlayDeleteConfirmComponent implements OnInit {
  @Input() note: any;

  @Output('resolved')
  resolveEvent = new EventEmitter<any>();

  fetching = false;

  constructor(private eventEmitter: EventEmitterService, private api: ApiService, private router: Router) { }

  ngOnInit() {
  }

  async delete() {
    this.fetching = true;

    let res;
    try {
      res = await this.api.request('notes/delete', {
        note_id: this.note.id
      }, true);
    } catch (e) {
      res = { success: false };
    }

    this.fetching = false;

    if (res.success) {
      this.eventEmitter.pushNotif({
        icon: 'fas fa-trash',
        details: 'Note deleted!',
        color: 'var(--color-green)'
      });
      if (location.href.indexOf('/edit') > -1) { // If the user is in edit mode
        this.router.navigate(['u/' + this.api.session.user_id]); // Return to user listing
      }
      this.resolveEvent.emit(true);
    } else {
      this.eventEmitter.pushNotif({
        icon: 'fas fa-trash',
        details: 'Failed to delete',
        color: 'var(--color-red)'
      });
    }
  }

  cancel() {
    this.resolveEvent.emit(false);
  }

}
