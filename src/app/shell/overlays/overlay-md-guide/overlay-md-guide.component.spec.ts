import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayMdGuideComponent } from './overlay-md-guide.component';

describe('OverlayMdGuideComponent', () => {
  let component: OverlayMdGuideComponent;
  let fixture: ComponentFixture<OverlayMdGuideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlayMdGuideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayMdGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
