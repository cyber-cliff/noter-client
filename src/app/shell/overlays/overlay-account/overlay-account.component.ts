import {Component, OnInit} from '@angular/core';
import {ApiService} from 'src/app/api.service';
import {EventEmitterService} from 'src/app/event-emitter.service';
import {OverlayService} from '../overlay.service';

@Component({
    selector: 'app-overlay-account',
    templateUrl: './overlay-account.component.html',
    styleUrls: ['./overlay-account.component.scss']
})
export class OverlayAccountComponent implements OnInit {
    username = this.api.username;

    fetching = false;
    res: any;

    createdAtDelta: string;
    activeAtDelta: string;

    constructor(private api: ApiService, private overlayService: OverlayService, private eventEmitter: EventEmitterService) {
    }

    ngOnInit() {
        this.fetchProfile();
    }

    async fetchProfile() {
        this.fetching = true;
        this.res = await this.api.request('users/profile/get', {user_id: this.api.session.user_id}, true);

        if (this.res.success) {
            this.createdAtDelta = this.calcDateDeltaString(this.res.data.created_at);
            this.activeAtDelta = this.calcDateDeltaString(this.res.data.active_at);
        }

        this.fetching = false;
    }

    logOut() {
        this.api.logOut();
        this.overlayService.cancelOverlay();
    }

    async deauthAllSessions() {
        const res = await this.api.request('auth/destroy', {}, true);
        if (res.success) {
            this.eventEmitter.pushNotif({
                icon: 'fas fa-user-slash',
                details: 'All sessions for your account have been invalidated! You will be now logged out.',
                color: 'var(--color-green)',
                duration: 20000
            });
            this.logOut();
        }
    }

    openSettings() {
        this.overlayService.showOverlay('settings');
    }

    calcDateDeltaString(time: number) {
        if (time <= 1576164788708) {
            return '∞ days ago';
        }
        time = new Date().getTime() - time; // Get delta

        const minutes = time / 60 / 1000;
        const hours = minutes / 60;
        const days = hours / 24;

        let final;
        if (Math.floor(minutes) > 1) {
            final = Math.floor(minutes) + ' minutes ago';
        }
        if (Math.floor(minutes) === 1) {
            final = Math.floor(minutes) + ' minute ago';
        }

        if (Math.floor(hours) > 1) {
            final = Math.floor(hours) + ' hours ago';
        }
        if (Math.floor(hours) === 1) {
            final = Math.floor(hours) + ' hour ago';
        }

        if (Math.floor(days) > 1) {
            final = Math.floor(days) + ' days ago';
        }
        if (Math.floor(days) === 1) {
            final = Math.floor(days) + ' day ago';
        }

        if (!final) {
            final = 'a few seconds ago';
        }

        return final;
    }
}
