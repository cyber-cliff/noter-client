import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlaySearchComponent } from './overlay-search.component';

describe('OverlaySearchComponent', () => {
  let component: OverlaySearchComponent;
  let fixture: ComponentFixture<OverlaySearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlaySearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlaySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
