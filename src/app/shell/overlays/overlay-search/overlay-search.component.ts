import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../api.service';

@Component({
    selector: 'app-overlay-search',
    templateUrl: './overlay-search.component.html',
    styleUrls: ['./overlay-search.component.scss']
})
export class OverlaySearchComponent implements OnInit {

    searchTerm: string;

    fetching = false;
    res: any;

    lastInputEditTimeout: number;

    constructor(
        public api: ApiService
    ) {
    }

    ngOnInit(): void {
    }

    async onSubmit() {
        return;
    }

    checkEdit() { // thx https://stackoverflow.com/a/5926782
        clearInterval(this.lastInputEditTimeout);

        if (this.searchTerm) {
            this.fetching = true;
            this.lastInputEditTimeout = setTimeout(() => { this.fetchSearch(); }, 500);
        }
    }

    async fetchSearch() {
        const res = await this.api.request('search', {
            phrase: this.searchTerm,
            users: true,
            published_notes: true,
            user_notes: !!this.api.session.token
        }, !!this.api.session.token);
        this.fetching = false;
        this.res = res;
    }

}
