import { Component, OnInit, VERSION } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { OverlayService } from '../overlay.service';
import { SettingsService } from 'src/app/services/settings.service';
import { ThemeService } from 'src/app/theme/theme.service';
import { EventEmitterService } from 'src/app/event-emitter.service';
import { LogService } from 'src/app/services/log.service';
@Component({
  selector: 'app-overlay-settings',
  templateUrl: './overlay-settings.component.html',
  styleUrls: ['./overlay-settings.component.scss']
})
export class OverlaySettingsComponent implements OnInit {
  serverInfo: any;
  baseUrl = this.api.BASE_API_ADDRESS;

  angularVersion = VERSION.full;

  fetchFailed = false;

  dropdownStates = {
    credits: false
  };

  constructor(
    private api: ApiService,
    private overlayService: OverlayService,
    private eventEmitter: EventEmitterService,
    private log: LogService,
    public settings: SettingsService,
    public theme: ThemeService
  ) {
    this.fetchServerInfo();
  }

  ngOnInit() {
  }

  save() {
    this.settings.saveSettings();
  }

  async fetchServerInfo() {
    const res = await this.api.request('info/about', {});

    if (!res.success) {
      this.fetchFailed = true;
      return;
    }

    this.serverInfo = res.data;
  }

  changeColorTagHeadings(value: boolean) {
    if (value) {
      localStorage.removeItem('appearanceDontColorTagHeadings');
    } else {
      localStorage.appearanceDontColorTagHeadings = true
    }
  }

  showDebugOverlay() {
    this.overlayService.showOverlay('debug');
  }

  openExternalLink(link: string) {
    window.open(link, '_blank');
  }

  resetLs() {
    localStorage.clear();
    sessionStorage.clear();
    location.reload();
  }

  //* Debug commands only bellow

  async reload() {
    location.reload(true);
  }

  async authCheck() {
    const res = await this.api.authSession();

    if (res.success && res.data.valid) {
      this.eventEmitter.pushNotif({
        icon: 'fas fa-bug',
        details: `Auth check successful!`,
        color: 'var(--color-green)'
      });
    } else {
      this.eventEmitter.pushNotif({
        icon: 'fas fa-bug',
        details: `Auth check failed!`,
        color: 'var(--color-red)'
      });
    }
  }

  async forceArl() {
    await this.api.autoLogin();
  }

  async dumpSessionInfo() {
    this.eventEmitter.pushNotif({
      icon: 'fas fa-bug',
      details: `
        *Warning! Giving this information will grant that person access to your account! Treat the tokens bellow as your password!*

        **Session Token:** \`${this.api.session.token}\`
        **Session UserID:** \`${this.api.session.user_id}\`
        **Persistent Token:** \`${localStorage.n_pt}\`
      `,
      color: 'var(--color-lblue)',
      duration: 0
    });
  }

  clearStorage(which: string) {
    switch (which) {
      case 'local':
        localStorage.clear();
        this.eventEmitter.pushNotif({
          icon: 'fas fa-bug',
          details: `localStorage wiped!`,
          color: 'var(--color-green)'
        });
        break;
      case 'session':
        sessionStorage.clear();
        this.eventEmitter.pushNotif({
          icon: 'fas fa-bug',
          details: `sessionStorage wiped!`,
          color: 'var(--color-green)'
        });
        break;
      case 'settings':
        localStorage.n_settings = undefined;
        this.eventEmitter.pushNotif({
          icon: 'fas fa-bug',
          details: `n_settings is now undefined. Still time to save it by saving current settings by changing something ;)`,
          color: 'var(--color-green)'
        });
        break;
    }
  }

  simDg(underVersion?: boolean) {
    let downgradeVersion;

    if (underVersion) {
      downgradeVersion = 1;
    } else {
      downgradeVersion = this.settings.noterVersion.resetUnderVersion;
    }
    this.settings.values.version = downgradeVersion;
    this.settings.saveSettings();

    this.eventEmitter.pushNotif({
      icon: 'fas fa-bug',
      // tslint:disable-next-line:max-line-length
      details: `
        Noter version downgraded to ${downgradeVersion}!
        ${(underVersion) ? 'You are going to have your storages wiped after refresh!' : ''}`,
      color: 'var(--color-green)'
    });
  }

  pushTestNotif() {
    this.eventEmitter.pushNotif({
      icon: 'fas fa-bug',
      details: `Debug notification
      that can span
      multiple
      lines
      that can be very very very very very very very very very very very very very very very very very very very long and have
      veryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryverylongwords
      Now it also supports *markdown* like **this** and ***this***! \`Code input is here too...\``,
      color: 'var(--color-lblue)',
      duration: 0,
      actions: [
        {
          name: 'Test action',
          handler: () => {
            this.log.pushLog({
              type: 'info',
              msg: `Debug notif action executed`
            });
          }
        }
      ]
    });
  }
}
