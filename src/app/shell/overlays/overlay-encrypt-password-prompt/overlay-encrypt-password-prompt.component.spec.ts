import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayEncryptPasswordPromptComponent } from './overlay-encrypt-password-prompt.component';
import { FormBuilder } from '@angular/forms';

describe('OverlayEncryptPasswordPromptComponent', () => {
  let component: OverlayEncryptPasswordPromptComponent;
  let fixture: ComponentFixture<OverlayEncryptPasswordPromptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        FormBuilder
      ],
      declarations: [ OverlayEncryptPasswordPromptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayEncryptPasswordPromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
