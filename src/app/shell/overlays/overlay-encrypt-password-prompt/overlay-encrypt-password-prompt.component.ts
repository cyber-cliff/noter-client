import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-overlay-encrypt-password-prompt',
  templateUrl: './overlay-encrypt-password-prompt.component.html',
  styleUrls: ['./overlay-encrypt-password-prompt.component.scss']
})
export class OverlayEncryptPasswordPromptComponent implements OnInit {

  @Input() data: {
    type: string,
    encryptedData: string
  };

  @Output()
  resolved = new EventEmitter<{
    success: boolean,
    encryptionPassword: string,
    decryptedData: any
  }>();

  loading = false;

  passwordForm = this.fb.group({
    password: ['', Validators.required]
  });

  invalidPassword = false;

  @ViewChild('focus') focusElement: ElementRef;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.focusElement.nativeElement.focus();
    }, 25);
  }

  resetError() {
    this.invalidPassword = false;
  }

  onSubmit() {
    const pass = this.passwordForm.value.password;
    let decryptedData = CryptoJS.AES.decrypt(this.data.encryptedData, pass);
    try {
      decryptedData = decryptedData.toString(CryptoJS.enc.Utf8);
      if (!decryptedData) { throw ('no data'); }
    } catch (e) {
      this.invalidPassword = true;
      return;
    }


    if (this.data.type === 'AES_JSON') {
      decryptedData = JSON.parse(decryptedData);
    }

    if (decryptedData) {
      this.resolved.emit({
        success: true,
        encryptionPassword: pass,
        decryptedData
      });
    }
  }

}
