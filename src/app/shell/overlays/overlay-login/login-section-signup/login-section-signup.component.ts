import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ApiService } from 'src/app/api.service';
import { EventEmitterService } from 'src/app/event-emitter.service';

@Component({
  selector: 'app-login-section-signup',
  templateUrl: './login-section-signup.component.html',
  styleUrls: ['./login-section-signup.component.scss']
})
export class LoginSectionSignupComponent implements OnInit {
  loading = false;
  error: any;

  dataValid = false;
  username = '';
  password = '';
  passwordAgain = '';

  constructor(private fb: FormBuilder, private api: ApiService, private eventEmitter: EventEmitterService) { }

  ngOnInit() {
  }

  onInputChange() {
    this.dataValid =
      this.username.length > 0 &&
      this.password.length > 0 &&
      this.checkPasswords() &&
      this.isUsernameValid(this.username);
  }

  checkPasswords() {
    return this.password === this.passwordAgain;
  }

  isUsernameValid(username: string) {
    let UN_ALLOWED_CHARACTERS = ['_'];
    UN_ALLOWED_CHARACTERS = UN_ALLOWED_CHARACTERS.concat('1234567890'.split(''));
    UN_ALLOWED_CHARACTERS = UN_ALLOWED_CHARACTERS.concat('abcdefghijklmnopqrstuvwxyz'.split(''));
    UN_ALLOWED_CHARACTERS = UN_ALLOWED_CHARACTERS.concat('abcdefghijklmnopqrstuvwxyz'.toLocaleUpperCase().split(''));

    for (const letter of username.split('')) {
      if (!UN_ALLOWED_CHARACTERS.includes(letter)) {
        return false;
      }
    }

    return true;
  }

  async onSubmit() {
    if (!this.dataValid) { return false; }

    this.loading = true;
    this.error = undefined;

    const res = await this.api.register(this.username, this.password);

    this.loading = false;

    if (res.success) {
      this.eventEmitter.pushNotif({
        icon: 'fas fa-check',
        details: 'Successfully registered. You can now log in.',
        color: 'var(--color-green)'
      });


    } else {
      this.eventEmitter.pushNotif({
        icon: 'fas fa-times-circle',
        details: 'Failed to sign up.',
        color: 'var(--color-red)'
      });
      this.error = res.error;
    }
  }

}
