import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginSectionSignupComponent } from './login-section-signup.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';

describe('LoginSectionSignupComponent', () => {
  let component: LoginSectionSignupComponent;
  let fixture: ComponentFixture<LoginSectionSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      providers: [
        FormBuilder // TODO: get rid of this once item input replace FB
      ],
      declarations: [ LoginSectionSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginSectionSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fail when any field is empty', () => {
    component.username = '';
    component.password = '';
    component.passwordAgain = '';
    component.onInputChange();
    expect(component.dataValid).toBeFalsy();

    component.username = 'username';
    component.onInputChange();
    expect(component.dataValid).toBeFalsy();

    component.username = '';
    component.password = 'password';
    component.passwordAgain = 'password';
    component.onInputChange();
    expect(component.dataValid).toBeFalsy();
  });

  it('should fail when username has invalid format', () => {
    component.username = 'not a a@lid user`namé';
    component.password = 'password can be whatever';
    component.passwordAgain = 'password can be whatever';
    component.onInputChange();
    expect(component.dataValid).toBeFalsy();
  });

  it('should fail when passwords are not same', () => {
    component.username = 'valid_username';
    component.password = 'password can be whatever';
    component.passwordAgain = 'password can be whatever but different';
    component.onInputChange();
    expect(component.dataValid).toBeFalsy();
  });

  it('should be valid', () => {
    component.username = 'valid_username1234567890ABC';
    component.password = 'password can be whatever';
    component.passwordAgain = 'password can be whatever';
    component.onInputChange();
    expect(component.dataValid).toBeTruthy();
  });
});
