import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginSectionLoginComponent } from './login-section-login.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('LoginSectionLoginComponent', () => {
  let component: LoginSectionLoginComponent;
  let fixture: ComponentFixture<LoginSectionLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [LoginSectionLoginComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginSectionLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fail when any field is empty', () => {
    component.onInputChange();
    expect(component.dataValid).toBeFalsy();

    component.username = 'username';
    component.onInputChange();
    expect(component.dataValid).toBeFalsy();

    component.username = '';
    component.password = 'password';
    component.onInputChange();
    expect(component.dataValid).toBeFalsy();
  });

  it('should fail when username has invalid format', () => {
    component.username = 'not a a@lid user`namé';
    component.password = 'password can be whatever';
    component.onInputChange();
    expect(component.dataValid).toBeFalsy();
  });

  it('should be valid', () => {
    component.username = 'valid_username1234567890ABC';
    component.password = 'password can be whatever';
    component.onInputChange();
    expect(component.dataValid).toBeTruthy();
  });
});
