import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EventEmitterService } from 'src/app/event-emitter.service';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-login-section-login',
  templateUrl: './login-section-login.component.html',
  styleUrls: ['./login-section-login.component.scss']
})
export class LoginSectionLoginComponent implements OnInit {

  loading = false;
  error: any;

  username = '';
  password = '';

  dataValid = false;

  constructor(private api: ApiService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  async onSubmit() {
    if (!this.dataValid) { return false; }

    this.loading = true;
    this.error = undefined;

    const username = this.username;
    const password = this.password;

    const res = await this.api.manualLogin(username, password);

    this.loading = false;

    if (!res.success) {
      this.error = res.error;
    }
  }

  onInputChange() {
    this.dataValid =
      this.username.length > 0 &&
      this.password.length > 0 &&
      this.isUsernameValid(this.username);

    // Reset any outdated errors
    this.error = undefined;
  }

  isUsernameValid(username: string) {
    let UN_ALLOWED_CHARACTERS = ['_'];
    UN_ALLOWED_CHARACTERS = UN_ALLOWED_CHARACTERS.concat('1234567890'.split(''));
    UN_ALLOWED_CHARACTERS = UN_ALLOWED_CHARACTERS.concat('abcdefghijklmnopqrstuvwxyz'.split(''));
    UN_ALLOWED_CHARACTERS = UN_ALLOWED_CHARACTERS.concat('abcdefghijklmnopqrstuvwxyz'.toLocaleUpperCase().split(''));

    for (const letter of username.split('')) {
      if (!UN_ALLOWED_CHARACTERS.includes(letter)) {
        return false;
      }
    }

    return true;
  }

}
