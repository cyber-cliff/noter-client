import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from 'src/app/api.service';
import { EventEmitterService } from 'src/app/event-emitter.service';

@Component({
  selector: 'app-overlay-login',
  templateUrl: './overlay-login.component.html',
  styleUrls: ['./overlay-login.component.scss']
})
export class OverlayLoginComponent implements OnInit {
  @Input() data: any;
  section = 'login';

  constructor(private fb: FormBuilder, public api: ApiService, private eventEmitter: EventEmitterService) { }

  ngOnInit() {
    if (this.api.failedAutoLogin) {
      this.section = 'autologinfail';
    }

    if (this.data && this.data.section) {
      this.section = this.data.section;
    }
  }

  retryAutoLogin() {
    this.api.autoLogin();
  }

  wipReg() {
    this.eventEmitter.pushNotif({
      icon: 'fas fa-times-circle',
      details: 'You cannot register at this time since Noter is in In-Dev state.',
      color: 'var(--color-orange)',
      duration: 15000
    });
  }
  wipFog() {
    this.eventEmitter.pushNotif({
      icon: 'fas fa-times-circle',
      details: 'Resetting password is not implemented yet.',
      color: 'var(--color-orange)',
      duration: 15000
    });
  }
}
