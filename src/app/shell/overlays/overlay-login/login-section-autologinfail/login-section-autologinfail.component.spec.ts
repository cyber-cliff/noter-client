import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginSectionAutologinfailComponent } from './login-section-autologinfail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('LoginSectionAutologinfailComponent', () => {
  let component: LoginSectionAutologinfailComponent;
  let fixture: ComponentFixture<LoginSectionAutologinfailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [ LoginSectionAutologinfailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginSectionAutologinfailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
