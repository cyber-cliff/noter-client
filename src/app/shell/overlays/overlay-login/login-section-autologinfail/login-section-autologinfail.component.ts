import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { OverlayService } from '../../overlay.service';

@Component({
  selector: 'app-login-section-autologinfail',
  templateUrl: './login-section-autologinfail.component.html',
  styleUrls: ['./login-section-autologinfail.component.scss']
})
export class LoginSectionAutologinfailComponent implements OnInit {

  fetching = false;

  constructor(private api: ApiService, private overlayService: OverlayService) { }

  ngOnInit() {
  }

  async retryAL() {
    this.fetching = true;
    await this.api.autoLogin();
    if (!this.api.failedAutoLogin) {
      this.overlayService.cancelOverlay();
    }
    this.fetching = false;
  }

}
