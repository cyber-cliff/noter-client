import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayLoginComponent } from './overlay-login.component';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('OverlayLoginComponent', () => {
  let component: OverlayLoginComponent;
  let fixture: ComponentFixture<OverlayLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      providers: [
        FormBuilder
      ],
      declarations: [ OverlayLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
