import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ApiService } from 'src/app/api.service';
import { EventEmitterService } from 'src/app/event-emitter.service';
import { Router } from '@angular/router';
import { OverlayService } from '../overlay.service';

@Component({
  selector: 'app-overlay-new-note',
  templateUrl: './overlay-new-note.component.html',
  styleUrls: ['./overlay-new-note.component.scss']
})
export class OverlayNewNoteComponent implements OnInit {
  @ViewChild('focus', { static: true }) focusElement: ElementRef;

  dataValid = false;
  title = '';

  loading = false;

  type = 'doc';

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    private overlayService: OverlayService,
    private eventEmitter: EventEmitterService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  onInputChange() {
    this.dataValid = this.title.length > 0;
  }

  async onSubmit() {
    if (!this.dataValid) { return false; }

    this.loading = true;

    const res = await this.api.request('notes/create', {
      title: this.title,
      type: this.type
    }, true);

    this.loading = false;

    if (res.success) {
      this.router.navigate(['/n/' + res.data.note_id + '/edit']);

      this.overlayService.cancelOverlay();
    }
  }
}
