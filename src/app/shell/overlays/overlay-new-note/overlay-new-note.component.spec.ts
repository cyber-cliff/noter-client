import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayNewNoteComponent } from './overlay-new-note.component';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('OverlayNewNoteComponent', () => {
  let component: OverlayNewNoteComponent;
  let fixture: ComponentFixture<OverlayNewNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      providers: [
        FormBuilder
      ],
      declarations: [ OverlayNewNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayNewNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
