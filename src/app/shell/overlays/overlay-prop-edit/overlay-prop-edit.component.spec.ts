import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayPropEditComponent } from './overlay-prop-edit.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';
import { NotSavedGuard } from 'src/app/guards/not-saved.guard';

describe('OverlayPropEditComponent', () => {
  let component: OverlayPropEditComponent;
  let fixture: ComponentFixture<OverlayPropEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot(),
      ],
      providers: [
        FormBuilder, // TODO: remove this once FB is gone :crab:
        NotSavedGuard
      ],
      declarations: [ OverlayPropEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayPropEditComponent);
    component = fixture.componentInstance;
    component.note = {
      id: '5e0493f703aa92008108b8c4',
      author: '5df25db44fd1f104b04a70d4',
      title: 'Otec Goriot',
      tags: [
        '5df51eed372de607f41ec914'
      ],
      created_at: 1577358327927,
      modified_at: 1584360016802,
      access: 3,
      encrypted: false,
      doc: {
        head: {
          ndfVersion: 4,
          type: 'doc'
        },
        body: {
          sections: [
            {
              type: 'markdown',
              content: '# Some content'
            }
          ]
        }
      }
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
