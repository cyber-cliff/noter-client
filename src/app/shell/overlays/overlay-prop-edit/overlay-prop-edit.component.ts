import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EventEmitterService } from 'src/app/event-emitter.service';
import { ApiService } from 'src/app/api.service';
import { OverlayService } from '../overlay.service';
import { Router } from '@angular/router';
import { Validators, FormBuilder } from '@angular/forms';

import { NotSavedGuard } from 'src/app/guards/not-saved.guard';
import { NoteEditorComponent } from 'src/app/pages/note-editor/note-editor.component';
import { StateService } from 'src/app/services/state.service';

import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-overlay-prop-edit',
  templateUrl: './overlay-prop-edit.component.html',
  styleUrls: ['./overlay-prop-edit.component.scss']
})
export class OverlayPropEditComponent implements OnInit {
  @Input() note: any;

  @Output('resolved')
  resolveEvent = new EventEmitter<any>();
  @Output('returnTrueOnCancel')
  returnTrueOnCancelEvent = new EventEmitter<any>();

  newTagForm = this.fb.group({
    name: ['', Validators.required],
    color: ['', Validators.required]
  });

  baseAddress = location.origin;

  fetching = true;
  fetchSuccess = false;

  tags = [];

  noteBefore: string;

  lastInputEditTimeout: any;

  // Encryption related stuff

  password: string;
  passwordAgain: string;
  encrypting = false;

  constructor(
    private eventEmitter: EventEmitterService,
    private overlayService: OverlayService,
    private router: Router,
    private fb: FormBuilder,
    private notSavedGuard: NotSavedGuard,
    public state: StateService,
    public api: ApiService
  ) {
    this.fetchTags();
  }

  ngOnInit() {
  }

  async fetchTags() {
    const res = await this.api.request('tags/get', {}, true);

    this.fetching = false;

    if (res.success) {
      this.fetchSuccess = true;
      this.noteBefore = JSON.stringify(this.note);

      if (!res.data) { this.tags = []; return; }
      this.tags = res.data.tags;
    }

  }

  checkEdit() { // thx https://stackoverflow.com/a/5926782
    clearInterval(this.lastInputEditTimeout);

    if (this.note.title) {
      this.lastInputEditTimeout = setTimeout(() => { this.save(); }, 500);
    }
  }

  // thx https://stackoverflow.com/questions/49102724/angular-5-copy-to-clipboard
  copyText(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.eventEmitter.pushNotif({
      icon: 'fas fa-copy',
      details: 'Link copied!',
      color: 'var(--color-green)',
      duration: 1500
    });
    this.resolveEvent.emit(false);
  }

  toggleTag(tagId) {
    if (this.note.tags.includes(tagId)) {
      this.note.tags.splice(this.note.tags.indexOf(tagId), 1);
    } else {
      this.note.tags.push(tagId);
    }

    this.save();
  }

  async delete() {
    await this.overlayService.showOverlay('delete-confirm', this.note);
    this.resolveEvent.emit(true);
  }

  setAccess(level) {
    // Don't allow setting access to nond and published on encrypted notes
    if (this.note.encrypted && level > 1) { return; }

    this.note.access = level;

    this.save();
  }

  async onNewTagSubmit() {
    const res = await this.api.request('tags/create', { name: this.newTagForm.value.name, color: this.newTagForm.value.color }, true);

    if (res.success) {
      this.newTagForm.patchValue({ name: '', color: undefined });

      await this.fetchTags();
    }
  }

  async deleteTag(tagId: number) {
    this.returnTrueOnCancelEvent.emit();

    this.tags = this.tags.filter((tag) => {
      return tag.id !== tagId;
    });

    const res = await this.api.request('tags/delete', { tag_id: tagId }, true);

    if (res.success) {
      await this.fetchTags();
    }
  }

  async save() {
    this.returnTrueOnCancelEvent.emit();
    const res = await this.api.request('notes/update', {
      note_id: this.note.id,
      note: {
        title: this.note.title,
        tags: this.note.tags,

        access: this.note.access,
      }
    }, true);
  }

  async encryptNote() {
    this.encrypting = true;

    if (!this.password || (this.password !== this.passwordAgain)) {
      this.encrypting = false;
      return;
    }

    if (this.note.access > 1) {
      this.encrypting = false;
      return;
    }

    const password = this.password;

    let res = await this.api.request('notes/get', {note_id: this.note.id}, true);
    if (!res.success) { return; }

    const doc = res.data.note.doc;

    if (doc.head.encryption) {
      console.log('Already encrypted.');
      this.encrypting = false;
      return;
    }

    doc.head.encryption = {
      type: 'AES_JSON'
    };

    doc.body = CryptoJS.AES.encrypt(JSON.stringify(doc.body), password).toString();

    res = await this.api.request('notes/update', {
      note_id: this.note.id,
      note: {
        doc
      }
    }, true);

    if (res.success) {
      this.note.encrypted = true;
      this.password = '';
      this.passwordAgain = '';
    }

    this.encrypting = false;
  }
}
