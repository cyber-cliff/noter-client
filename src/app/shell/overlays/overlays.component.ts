import {Component, OnInit} from '@angular/core';
import {OverlayService} from './overlay.service';

@Component({
    selector: 'app-overlays',
    templateUrl: './overlays.component.html',
    host: {'(window:keydown)': 'hotkeys($event)', '(window:keyup)': 'keyUp($event)'},
    styleUrls: ['./overlays.component.scss']
})
export class OverlaysComponent implements OnInit {

    overlayStates = {
        'note-index': false,

        'delete-confirm': false,
        'prop-edit': false,
        'note-save': false,

        'list-grouping': false,

        'md-guide': false,

        'encrypt-password-prompt': false,

        'new-note': false,
        'open': false,
        'tasks': false,

        'debug': false,
        'login': false,
        'user': false,
        'account': false,
        'settings': false,
        'search': false
    };

    sidebarOverlays = [ // So the overlay cover knows where to start
        'new-note',
        'open',
        'tasks',
        'debug',
        'login',
        'user',
        'account',
        'settings',
        'search'
    ];

    overlayData: any;
    returnTrueOnCancel = false;

    showCover = false;
    reverseCover = false;

    hotkeysData = [
        {
            code: 'Escape', canInput: true, handler: () => {
                if (this.showCover) {
                    this.resolveData(this.returnTrueOnCancel);
                }
            }
        },
        {
            code: 'struct', canInput: false, ctrl: false, alt: false, shift: false, handler: () => {

            }
        }
    ];

    constructor(private overlayService: OverlayService) {
        if (this.overlayService.overlaySub === undefined) {
            this.overlayService.overlaySub = this.overlayService.overlayEmitter.subscribe((data) => {
                if (data.type === 'show') {

                    this.hideAllOverlays();

                    this.overlayData = data.data;
                    this.overlayStates[data.name] = true;
                    if (this.sidebarOverlays.includes(data.name)) {
                        this.reverseCover = true;
                    } else {
                        this.reverseCover = false;
                    }

                    this.calcCoverState();
                } else if (data.type === 'cancel') {
                    this.resolveData(this.returnTrueOnCancel);
                }
            });
        }
    }

    resolveData(data) {
        this.overlayService.resolverEmitter.emit(data);

        this.hideAllOverlays();
    }

    hideAllOverlays() {
        this.returnTrueOnCancel = false; // Reset for next use
        for (const overlay of Object.keys(this.overlayStates)) {
            this.overlayStates[overlay] = false;
            this.overlayData = undefined;
            this.calcCoverState();
        }
    }

    calcCoverState() {
        this.showCover = false;
        for (const overlayState of Object.values(this.overlayStates)) {
            if (overlayState) {
                this.showCover = true;
            }
        }
    }

    setupReturnTrueOnCancel() {
        this.returnTrueOnCancel = true;
    }

    ngOnInit() {
    }

    hotkeys(event) {
        // if (this.wasKeyDown) { return; }

        for (const hotkey of this.hotkeysData) {
            if (
                event.code === hotkey.code &&
                event.ctrlKey === !!hotkey.ctrl &&
                event.altKey === !!hotkey.alt &&
                event.shiftKey === !!hotkey.shift
            ) {
                if (event.srcElement.localName === 'input' && !(hotkey.canInput || !!hotkey.ctrl || !!hotkey.alt)) {
                    return;
                }
                if (!!event.srcElement.attributes.contenteditable && !(hotkey.canInput || !!hotkey.ctrl || !!hotkey.alt)) {
                    return;
                }

                event.preventDefault();

                hotkey.handler();
                return false;
            }
        }
    }

    keyUp(event) {
    }

}
