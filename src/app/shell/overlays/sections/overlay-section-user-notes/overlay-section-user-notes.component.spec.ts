import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlaySectionUserNotesComponent } from './overlay-section-user-notes.component';

describe('OverlaySectionUserNotesComponent', () => {
  let component: OverlaySectionUserNotesComponent;
  let fixture: ComponentFixture<OverlaySectionUserNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlaySectionUserNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlaySectionUserNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
