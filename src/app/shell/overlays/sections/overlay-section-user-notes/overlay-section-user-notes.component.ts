import {AfterContentInit, Component, Input, OnInit} from '@angular/core';
import {ApiService} from '../../../../api.service';

@Component({
    selector: 'app-overlay-section-user-notes',
    templateUrl: './overlay-section-user-notes.component.html',
    styleUrls: ['./overlay-section-user-notes.component.scss']
})
export class OverlaySectionUserNotesComponent implements AfterContentInit {

    @Input()
    userId: string;

    userFetching = false;
    userRes: {
        success: boolean,
        data?: any,
        error?: any
    };

    tagsFetching = false;
    tagsRes: {
        success: boolean,
        data?: any,
        error?: any
    };

    constructor(
        public api: ApiService
    ) {
    }

    ngAfterContentInit(): void {
        this.fetchUser();
        this.fetchTags();
    }

    async fetchUser() {
        this.userFetching = true;
        this.userRes = await this.api.request('users/notes/get', {user_id: this.userId}, true);
        this.userFetching = false;
    }
    async fetchTags() {
        this.tagsFetching = true;
        this.tagsRes = await this.api.request('tags/get', {user_id: this.userId}, true);
        this.tagsFetching = false;
    }

}
