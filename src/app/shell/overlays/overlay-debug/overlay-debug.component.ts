import { Component, OnInit, OnChanges, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { EventEmitterService } from 'src/app/event-emitter.service';
import { SettingsService } from 'src/app/services/settings.service';
import { LogService } from 'src/app/services/log.service';
import { OverlayService } from '../overlay.service';
import { ThemeService } from 'src/app/theme/theme.service';
import { DrawCoderService } from 'src/app/services/draw-coder.service';
import { StateService } from 'src/app/services/state.service';

@Component({
  selector: 'app-overlay-debug',
  templateUrl: './overlay-debug.component.html',
  styleUrls: ['./overlay-debug.component.scss']
})
export class OverlayDebugComponent implements OnInit, OnChanges {
  currentCommand = '';

  @ViewChild('view', { static: true }) viewElement: ElementRef;
  @ViewChild('focus', { static: true }) focusElement: ElementRef;

  commands = [
    {
      keyword: 'clear',
      desc: 'Clears all logs',
      handler: async () => {
        this.log.log = [];

        return;
      }
    },

    {
      keyword: 'st',
      desc: 'Jump to log start',
      handler: async () => {
        this.viewElement.nativeElement.scrollTop = 0;
        return;
      }
    },

    {
      keyword: 'exit',
      desc: 'Closes all overlays (including debug)',
      handler: async () => {
        this.overlayService.cancelOverlay();
        return;
      }
    },

    {
      keyword: 'sover',
      desc: 'Shows overlay based on the provided key',
      handler: async (params: Array<string>) => {
        console.log(params);
        const res = await this.overlayService.showOverlay(params[0], JSON.parse(params[1]));
        this.log.pushLog({
          type: 'none',
          msg: `Overlay returned:\n${JSON.stringify(res, null, 2)}`
        });
      }
    },

    {
      keyword: 'progsim',
      desc: 'Sets fetching state depending on the added param for 10 seconds (endpoint will be still undefined)',
      handler: async (params: Array<string>) => {
        this.stateService.apiFetching.fetching = true;
        this.stateService.apiFetching.direction = params[0];

        setTimeout(() => {
          this.stateService.apiFetching.fetching = false;
          this.stateService.apiFetching.direction = undefined;
        }, 10000);
      }
    },

    {
      keyword: 'ptlogin',
      desc: 'Allows signing in with a persistent token (DEV PURPOSES ONLY!)',
      handler: async (params: Array<string>) => {
        this.log.pushLog({
          type: 'warn',
          // tslint:disable-next-line:max-line-length
          msg: `Logging in with a persistent token without the consent of the account owner is against ToS and will get you most likely banned! Use ONLY when you know what are you doing!!!`
        });

        this.log.pushLog({
          type: 'info',
          msg: `Logging out...`
        });

        this.api.logOut();
        this.overlayService.showOverlay('debug');

        this.log.pushLog({
          type: 'info',
          msg: `Signing in as '${params[0]}'...`
        });

        const res = await this.api.ptLogin(params[0], params[1]);

        this.log.pushLog({
          type: 'none',
          msg: `Response from ptLogin:\n\n${res}`
        });
      }
    },

    {
      keyword: 'trapsarenotgae',
      handler: () => {
        this.log.pushLog({
          type: 'warn',
          msg: `Constant accent color changing may SERIOUSLY impact performance. USe only for debugging accent color :)`
        });
        this.theme.rainbowCycle();
      }
    },

    {
      keyword: 'help',
      desc: 'Displays list and meanings of commands',
      handler: () => {
        for (const command of this.commands) {
          if (!command.desc) { continue; }

          this.log.pushLog({
            type: 'none',
            msg: `${command.keyword} - ${command.desc}`
          });
        }
      }
    },

    {
      keyword: '!',
      desc: 'Repeat the last command executed',
      handler: () => {
        if (!localStorage.n_debug_command_recent) { return; }
        this.currentCommand = localStorage.n_debug_command_recent;
        this.execCommand();
      }
    },

    {
      keyword: 'export',
      desc: 'Exports log info to JSON',
      handler: () => {
        // thx https://stackoverflow.com/a/50376496
        const jsonData = JSON.stringify(this.log.log);
        const blob = new Blob([jsonData], { type: 'text/json' });
        const url = window.URL.createObjectURL(blob);

        const fileName = 'n_dlog_' + new Date().getTime() + '.json';

        if (navigator.msSaveOrOpenBlob) {
          navigator.msSaveBlob(blob, fileName);
        } else {
          const a = document.createElement('a');
          a.href = url;
          a.download = fileName;
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
        }
        window.URL.revokeObjectURL(url);

        this.log.pushLog({
          type: 'success',
          msg: `Exported debug log as '${fileName}'`
        });
      }
    },
  ];

  constructor(
    private api: ApiService,
    private eventEmitter: EventEmitterService,
    private settings: SettingsService,
    private overlayService: OverlayService,
    private theme: ThemeService,
    private drawCoder: DrawCoderService,
    private stateService: StateService,
    public log: LogService
  ) {

  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.viewElement.nativeElement.scrollTop = this.viewElement.nativeElement.scrollHeight;
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.focusElement.nativeElement.focus();
    }, 25);
    if (!localStorage.getItem('debugConsoleAccepted')) {
      this.eventEmitter.pushNotif({
        icon: 'fas fa-exclamation-triangle',
        // tslint:disable-next-line: max-line-length
        details: '**This console is only for debugging purposes!**\n\nIf you don\'t know what you\'re doing, or someone told you to type something here, do not use this console and press \`[ESC]\` to exit it.',
        color: 'var(--color-yellow)',
        duration: 0,
        onDismiss: () => { localStorage.setItem('debugConsoleAccepted', 'true'); }
      });
    }
  }

  async execCommand() {
    const currentCommandKeyword = this.currentCommand;
    this.currentCommand = '';

    if (!currentCommandKeyword.startsWith('!')) {
      localStorage.n_debug_command_recent = currentCommandKeyword;
    }

    this.log.pushLog({
      type: 'debug',
      msg: `> ${currentCommandKeyword}`
    });

    const selectedCommand = this.commands.filter((command) => {
      return currentCommandKeyword.startsWith(command.keyword);
    })[0];

    if (!selectedCommand) {
      this.log.pushLog({
        type: 'warn',
        msg: `Command '${currentCommandKeyword.split(' ')[0]}' does not exist.`
      });
      setTimeout(() => {
        this.viewElement.nativeElement.scrollTop = this.viewElement.nativeElement.scrollHeight;
      }, 25);
      return;
    }

    await selectedCommand.handler(currentCommandKeyword.split(' ').slice(1));

    if (currentCommandKeyword.split(' ')[0] !== 'st') {
      setTimeout(() => {
        this.viewElement.nativeElement.scrollTop = this.viewElement.nativeElement.scrollHeight;
      }, 25);
    }
  }
}
