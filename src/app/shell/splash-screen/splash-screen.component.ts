import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { StateService } from 'src/app/services/state.service';

@Component({
  selector: 'app-splash-screen',
  templateUrl: './splash-screen.component.html',
  styleUrls: ['./splash-screen.component.scss']
})
export class SplashScreenComponent implements OnInit {

  constructor(
    public api: ApiService,
    public stateService: StateService
  ) { }

  ngOnInit() {
  }

}
