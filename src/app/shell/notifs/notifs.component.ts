import {Component, OnInit} from '@angular/core';
import {EventEmitterService} from 'src/app/event-emitter.service';
import {MarkdownService} from 'ngx-markdown';

@Component({
    selector: 'app-notifs',
    templateUrl: './notifs.component.html',
    styleUrls: ['./notifs.component.scss']
})
export class NotifsComponent implements OnInit {
    notifs = [];

    constructor(private eventEmitter: EventEmitterService, private markdownService: MarkdownService) {
    }

    ngOnInit() {
        if (this.eventEmitter.notifSub == undefined) {
            this.eventEmitter.notifSub = this.eventEmitter.notifEmitter.subscribe(({eventType, notif}) => {
                if (eventType === 'push') {
                    this.pushNotif(notif);

                    if (notif.duration === 0) {
                        return;
                    }
                    if (!notif.duration) {
                        notif.duration = 5000;
                    }

                    setTimeout(() => {
                        this.closeNotif(notif);
                    }, notif.duration);
                } else if (eventType === 'dismissAll') {
                    this.dismissAll();
                }
            });
        }
    }

    pushNotif(notif) {
        // let temp = this.notifs.reverse();
        // temp.push(notif);
        // this.notifs = temp.reverse();
        this.notifs.push(notif);
    }

    async executeAction(notif, action) {
        const stayOpen = await action.handler();
        if (!stayOpen) {
            this.closeNotif(notif);
        }
    }

    closeNotif(notif) {
        notif['dying'] = true;
        if (notif.onDismiss) {
            notif.onDismiss();
        }
        setTimeout(() => this.notifs.splice(this.notifs.indexOf(notif), 1), 475);
    }

    dismissNotifManually(notif) {
        if (notif.disableClosing) { return; }
        notif['wasDismissedManually'] = true;
        this.closeNotif(notif);
    }

    dismissAll() {
        for (const notif of this.notifs) {
            if (notif.disableClosing) { continue; }
            this.closeNotif(notif);
        }
    }
}
