import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { EventEmitterService } from 'src/app/event-emitter.service';
import { TouchSequence } from 'selenium-webdriver';
import { OverlayService } from '../overlays/overlay.service';
import { SettingsService } from 'src/app/services/settings.service';
import { DeviceService } from 'src/app/services/device.service';
import { StateService } from 'src/app/services/state.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  userId: string;
  isLoggedIn = !!this.api.session.token;

  constructor(
    private eventEmitter: EventEmitterService,
    public api: ApiService,
    public settings: SettingsService,
    public overlayService: OverlayService,
    public device: DeviceService,
    public stateService: StateService
  ) { }

  ngOnInit() {
    if (this.eventEmitter.sidebarSub === undefined) {
      this.eventEmitter.sidebarSub = this.eventEmitter.sidebarEmitter.subscribe(({ eventType }) => {
        if (eventType === 'login') {
          this.isLoggedIn = true;
          this.userId = this.api.session.user_id;
        }
        if (eventType === 'logout') {
          this.isLoggedIn = false;
          this.userId = '';
        }
      });
    }
  }

  overlayAction(overlayName) {
    if (this.overlayService.overlayShown === overlayName) {
      this.overlayService.cancelOverlay();
      return;
    }
    this.overlayService.showOverlay(overlayName);
  }

  onLogin(res) {

  }
}
