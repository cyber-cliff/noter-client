import { Component } from '@angular/core';
import { EventEmitterService } from './event-emitter.service';
import { Router, NavigationStart } from '@angular/router';
import { ApiService } from './api.service';
import { ThemeService } from './theme/theme.service';
import { SettingsService } from './services/settings.service';
import { MetaService } from './services/meta.service';
import { OverlayService } from './shell/overlays/overlay.service';
import { TranslateService } from '@ngx-translate/core';
import { MarkdownService } from 'ngx-markdown';
import { StartupService } from './services/startup.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  host: { '(window:keydown)': 'hotkeys($event)', '(window:keyup)': 'keyUp($event)' },
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  hotkeysData = [
    // * SIDEBAR HOTKEYS
    {
      code: 'KeyQ', alt: true, shift: true, handler: () => {
        if (!this.api.session.token) { return; }
        this.overlayService.showOverlay('new-note');
      }
    },
    {
      code: 'KeyN', ctrl: true, handler: () => {
        if (!this.api.session.token) { return; }
        this.overlayService.showOverlay('new-note');
      }
    },
    {
      code: 'KeyA', alt: true, shift: true, handler: () => {
        this.overlayService.showOverlay('open');
      }
    },
    {
      code: 'KeyW', alt: true, shift: true, handler: () => {
        this.router.navigateByUrl('/browse');
      }
    },
    {
      code: 'KeyS', alt: true, shift: true, handler: () => {
        this.overlayService.showOverlay('search');
      }
    },
    {
      code: 'KeyZ', alt: true, shift: true, handler: () => {
        if (this.api.session.token) { return; }
        this.overlayService.showOverlay('login');
      }
    },
    {
      code: 'KeyX', alt: true, shift: true, handler: () => {
        this.overlayService.showOverlay('settings');
      }
    },
    {
      code: 'KeyC', alt: true, shift: true, handler: () => {
        if (!this.api.session.token) { return; }
        this.overlayService.showOverlay('account');
      }
    },
    {
      code: 'KeyF', alt: true, shift: true, handler: () => {
        this.overlayService.showOverlay('debug');
      }
    },

    {
      code: 'KeyD', ctrl: true, handler: () => {
        this.eventEmitter.dismissAllNotifs();
      }
    },

    {
      code: 'KeyS', ctrl: true, handler: () => {
        this.eventEmitter.saveNote(); // TODO: make this better?
      }
    },
    {
      code: 'struct', canInput: false, ctrl: false, alt: false, shift: false, handler: () => {

      }
    }
  ];

  fadeSplash = false;
  showSplash = true;
  wasKeyDown = false;

  constructor(
    private eventEmitter: EventEmitterService,
    private router: Router,

    private markdownService: MarkdownService,
    private startup: StartupService,
    public overlayService: OverlayService,
    public api: ApiService,
    public settings: SettingsService,
  ) {
  }

  async ngOnInit() {
    const startTime = new Date().getTime();

    const shouldContinueToStart = await this.startup.startup();

    if (!shouldContinueToStart) {
      return;
    }

    // Hide splash
    if (this.settings.values.startup_splash_delay === 'wait') {
      const delaySplashBy = 1000 - (new Date().getTime() - startTime);
      if (delaySplashBy <= 0) {
        this.hideSplashScreen(); // If the 2000ms passed already while loading, just hide the splash
        return;
      }
      // Otherwise wait the remaining time
      setTimeout(() => {
        this.hideSplashScreen();
      }, delaySplashBy);
    } else {
      this.hideSplashScreen();
    }
  }

  hideSplashScreen() {
    if (this.settings.values.startup_splash_delay === 'ultra') {
      this.showSplash = false;
      return;
    }

    this.fadeSplash = true;
    setTimeout(() => {
      this.showSplash = false;
    }, 300);
  }

  hotkeys(event) {
    // if (this.wasKeyDown) { return; }

    for (const hotkey of this.hotkeysData) {
      if (
        event.code === hotkey.code &&
        event.ctrlKey === !!hotkey.ctrl &&
        event.altKey === !!hotkey.alt &&
        event.shiftKey === !!hotkey.shift
      ) {
        if (event.srcElement.localName === 'input' && !(hotkey.canInput || !!hotkey.ctrl || !!hotkey.alt)) { return; }
        if (!!event.srcElement.attributes.contenteditable && !(hotkey.canInput || !!hotkey.ctrl || !!hotkey.alt)) { return; }

        event.preventDefault();

        hotkey.handler();
        this.wasKeyDown = true;
        return false;
      }
    }
  }

  keyUp(event) {
    this.wasKeyDown = false;
  }
}
