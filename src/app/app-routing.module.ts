import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserNotesComponent } from './pages/user/user-notes/user-notes.component';

import { NotFoundComponent } from './pages/errors/not-found/not-found.component';

import { NotSavedGuard } from './guards/not-saved.guard';
import { TestThemeComponent } from './pages/tests/test-theme/test-theme.component';
import { StatsComponent } from './pages/info/stats/stats.component';
import { BrowseComponent } from './pages/browse/browse.component';
import { DownloadedNotesComponent } from './pages/saved-notes/downloaded-notes/downloaded-notes.component';
import { BookmarkedNotesComponent } from './pages/saved-notes/bookmarked-notes/bookmarked-notes.component';
import { NoteViewerComponent } from './pages/note-viewer/note-viewer.component';
import { NoteEditorComponent } from './pages/note-editor/note-editor.component';
import { TasksComponent } from './pages/tasks/tasks.component';


const routes: Routes = [
  { path: 'n/:noteId', component: NoteViewerComponent },
  { path: 'n/:noteId/edit', component: NoteEditorComponent, canDeactivate: [NotSavedGuard] },

  { path: 'stats', component: StatsComponent },

  { path: 'debug/tests/theme', component: TestThemeComponent },

  { path: '', component: BrowseComponent },

  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
