import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  log = [
    { type: 'none', msg: 'Noter debug log' },
    { type: 'none', msg: '© FajsiEx 2019-2020' }, // TODO: add client version along with the version service
    { type: 'debug', msg: `Client init time: ${new Date().toLocaleString()} [${new Date().getTime()}]` },
    { type: 'warn', msg: 'Good luck debugging this thing :)' },
  ];

  constructor() { }

  pushLog(logEvent) {
    this.log.push(logEvent);
  }
}
