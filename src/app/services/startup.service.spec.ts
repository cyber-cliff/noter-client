import { TestBed } from '@angular/core/testing';

import { StartupService } from './startup.service';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { MarkdownModule } from 'ngx-markdown';
import { SwUpdate, ServiceWorkerModule } from '@angular/service-worker';

describe('StartupService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      TranslateModule.forRoot(),
      MarkdownModule.forRoot(),
      ServiceWorkerModule.register('', {enabled: false})
    ],
    providers: [
      SwUpdate
    ]
  }));

  it('should be created', () => {
    const service: StartupService = TestBed.get(StartupService);
    expect(service).toBeTruthy();
  });
});
