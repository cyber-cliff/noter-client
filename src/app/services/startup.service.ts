import {Injectable} from '@angular/core';
import {LogService} from './log.service';
import {SettingsService} from './settings.service';
import {MetaService} from './meta.service';
import {TranslateService} from '@ngx-translate/core';
import {Router, NavigationStart} from '@angular/router';
import {OverlayService} from '../shell/overlays/overlay.service';
import {MarkdownService} from 'ngx-markdown';
import {ApiService} from '../api.service';
import {NoteStorageService} from './note-storage.service';
import {EventEmitterService} from '../event-emitter.service';

import {SwUpdate} from '@angular/service-worker';
import {StateService} from './state.service';

@Injectable({
    providedIn: 'root'
})
export class StartupService {

    currentTarget: string;
    updating = false;
    startupFinsihed = false;
    activatingUpdatePromise;

    constructor(
        private log: LogService,
        private settings: SettingsService,
        private meta: MetaService,
        private translate: TranslateService,
        private router: Router,
        private markdownService: MarkdownService,
        private api: ApiService,
        private noteStore: NoteStorageService,
        private eventEmitter: EventEmitterService,
        private overlayService: OverlayService,
        private swUpdate: SwUpdate,
        private stateService: StateService
    ) {
    }

    async startup() {
        this.log.pushLog({msg: '[STARTUP] Starting up...', type: 'info'});

        console.log(this.stateService);
        const updatePromise = this.manageUpdates();
        const autoLoginPromise = this.api.autoLogin();

        this.logTarget('settings_load');
        this.settings.loadSettings();

        this.logTarget('note_store_load');
        this.noteStore.loadStore();

        this.logTarget('meta');
        this.meta.restoreDefault();

        this.logTarget('lang');
        this.translate.setDefaultLang('en');

        this.logTarget('qparams');
        this.detectQueryParams();

        this.logTarget('router_sub_setup');
        this.setupRouterSubs();

        this.logTarget('md_renderer_setup');
        this.setupMdRenderer();

        this.logTarget('auto_update');
        this.stateService.splashScreenText = 'Checking for updates...';
        await updatePromise;

        if (this.updating) {
            this.logTarget('> update_activate (waiting for reload!)');
            return false;
        }

        this.stateService.splashScreenText = 'Logging in...';

        this.logTarget('auto_login');
        await autoLoginPromise;

        if (localStorage.n_openNoterConsoleOnStartup === 'yes') {
            this.overlayService.showOverlay('debug');
            this.log.pushLog({msg: '[STARTUP] Here is your startup console ;)', type: 'info'});
        }

        this.logTarget('server_verify');
        this.api.pushServerInfoToLog();

        this.logTarget('show_notifs');
        this.showNotifs();

        this.logTarget('policy_update');
        this.stateService.splashScreenText = 'Checking for policy updates...';
        this.checkPolicyUpdates();

        this.logTarget('final');
        this.stateService.splashScreenText = ' ';
        this.startupFinsihed = true;
        return true;
    }

    detectQueryParams() {
        const queryParamsString = window.location.search.slice(1);
        const queryParamsStringsArray = queryParamsString.split('&');
        const queryParams = [];

        for (const qParam of queryParamsStringsArray) {
            queryParams[qParam.split('=')[0]] = qParam.split('=')[1];
        }

        console.log('Query parameters:', queryParams);
    }

    setupRouterSubs() {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                this.overlayService.cancelOverlay();

                if (event.url.indexOf('/edit') > -1) {
                    document.getElementById('favicon-meta').setAttribute('href', `/assets/favicons/edit.png`);

                } else if (event.url.indexOf('/n/') > -1) {
                    document.getElementById('favicon-meta').setAttribute('href', `/assets/favicons/note.png`);

                } else if (event.url.indexOf('/u/') > -1) {
                    document.getElementById('favicon-meta').setAttribute('href', `/assets/favicons/notes.png`);

                } else {
                    document.getElementById('favicon-meta').setAttribute('href', `/assets/favicon.png`);
                }
            }
        });
    }

    setupMdRenderer() {
        this.markdownService.renderer.listitem = (text: string) => {
            if (
                text.includes('<input checked="" disabled="" type="checkbox">') ||
                text.includes('<input disabled="" type="checkbox">')
            ) {
                text = text.replace('<input disabled="" type="checkbox">', '<i class="far fa-square task-indicator"></i>');
                text = text.replace('<input checked="" disabled="" type="checkbox">', '<i class="fas fa-check-square task-indicator active"></i>');
                return '<li class="task">' + text + '</li>\n'; // I need to do this until marked type doesn't include the new type defs :/
            } else {
                return '<li>' + text + '</li>\n';
            }
        };

        this.markdownService.renderer.paragraph = (text: string) => {
            return `<span class="paragraph">${text}</span>`;
        };
    }

    async showNotifs() {
        if (!localStorage.getItem('wipAccepted')) {
            this.eventEmitter.pushNotif({
                icon: 'fas fa-dragon',
                // tslint:disable-next-line: max-line-length
                details: `**Noter is in active development!** Things may not work or break whenever they want to. Here be dragons.`,
                color: 'var(--color-yellow)',
                duration: 0,
                onDismiss: () => {
                    localStorage.setItem('wipAccepted', 'true');
                }
            });
        }

        if (localStorage.getItem('overwriteApiBase') && !localStorage.getItem('n_overwrite_api_accepted')) {
            this.eventEmitter.pushNotif({
                icon: 'fas fa-exclamation-triangle',
                details: await this.translate.get('notifs.warnings.api_overwrite.text',
                    {address: localStorage.getItem('overwriteApiBase')})
                    .toPromise(),
                color: 'var(--color-orange)',
                duration: 0,
                actions: [
                    {
                        name: await this.translate.get('notifs.warnings.api_overwrite.actions.reset').toPromise(),
                        handler: async () => {
                            localStorage.removeItem('overwriteApiBase');

                            // Small delay to allow dismiss handler to set n_overwrite_api_accepted to true before we reset it
                            setTimeout(() => {
                                localStorage.removeItem('n_overwrite_api_accepted');
                                location.reload();
                            }, 250);
                        }
                    }
                ],
                onDismiss: () => {
                    localStorage.setItem('n_overwrite_api_accepted', 'true');
                }
            });
        }

        if (localStorage.getItem('n_display_underversion_notif')) {
            this.eventEmitter.pushNotif({
                icon: 'fas fa-exclamation-triangle',
                // tslint:disable-next-line: max-line-length
                details: 'Your settings were reset as the result of this update. Consult the changelog for more information about why this was done.',
                color: 'var(--color-orange)',
                duration: 0
            });
            // We do it here because we want it to display only once along with the update notif
            localStorage.removeItem('n_display_underversion_notif');
        }

        if (document.querySelector('.darkreader') && !localStorage.getItem('externalDarkenerAccepted')) {
            this.eventEmitter.pushNotif({
                icon: 'fas fa-exclamation-triangle',
                // tslint:disable-next-line: max-line-length
                details: '**It seems like you have an external darkener installed**\n*(e.g. darkreader)*.\nNoter has a dark theme by default and you should turn off your external darkener, if possible for the best experience!',
                color: 'var(--color-lblue)',
                duration: 0,
                onDismiss: () => {
                    localStorage.setItem('externalDarkenerAccepted', 'true');
                }
            });
        }
    }

    async manageUpdates() {
        if (this.swUpdate.isEnabled) {
            this.swUpdate.available.subscribe(async event => {
                if (this.updating) {
                    return;
                }

                this.updating = true;
                this.stateService.splashScreenText = 'Updating...';

                console.log('[SWUPD] New update available');

                await this.swUpdate.activateUpdate();

                if (this.startupFinsihed) {
                    this.eventEmitter.pushNotif({
                        icon: 'fas fa-sync-alt',
                        details: `Update available. Restart Noter to install it.`,
                        color: 'var(--color-lblue)',
                        duration: 0,
                        actions: [
                            {
                                name: 'Restart Noter',
                                handler: async () => {
                                    location.reload(true);
                                }
                            }
                        ]
                    });

                    console.log('[SWUPD] Update finished. Waiting for reload');
                } else {
                    // Since the startup is not finished yet, we will reload.
                    location.reload(true);
                }
            });

            await this.swUpdate.checkForUpdate();
        }
    }

    async checkPolicyUpdates() {
        const responses = await Promise.all([ // TODO: store ids somewhere else...
            this.api.request('notes/get', {note_id: '5e0296d171de4f00830bc84f'}, false, {disableTracking: true}), // ToS
            this.api.request('notes/get', {note_id: '5e01f836ade41b2b1c768ffc'}, false, {disableTracking: true}) // Privacy policy
        ]);

        let firstTime = false;
        let policyAcceptTimes;
        if (!localStorage.n_policy_accept_times) {
            policyAcceptTimes = {};
            firstTime = true;
        } else {
            policyAcceptTimes = JSON.parse(localStorage.n_policy_accept_times);
        }

        const updatedPolicies = [];

        for (const res of responses) {
            if (!res.success) {
                continue;
            } // If the fetch was not successful we just skip that response

            if (!policyAcceptTimes[res.data.note.id] || (policyAcceptTimes[res.data.note.id] < res.data.note.modified_at)) {
                console.warn(`Policy [${res.data.note.title}] was updated and not accepted.`);
                updatedPolicies.push(res.data.note);
            }
        }

        if (updatedPolicies.length === 0) {
            return;
        }

        let notifDesc;
        const actions = [];

        if (firstTime) {
            notifDesc = `By using Noter you accept to the following policies:\n`;
        } else {
            notifDesc = `The following policies were updated:\n`;
        }

        for (const policy of updatedPolicies) {
            notifDesc += `[${new Date(policy.modified_at).toLocaleDateString()}] **${policy.title}**\n`;
            actions.push({
                name: 'View ' + policy.title,
                handler: async () => {
                    await this.router.navigate(['/n/' + policy.id]);
                    return true;
                }
            });
        }


        this.eventEmitter.pushNotif({
            icon: 'fas fa-scroll',
            details: notifDesc + `\n\nIt is advised you read these policies. By using Noter in any way you accept these policies.`,
            color: 'var(--color-lblue)',
            duration: 0,
            disableClosing: true,
            actions: actions.concat([
                {
                    name: 'Accept all policies',
                    handler: async () => {
                        for (const policy of updatedPolicies) {
                            policyAcceptTimes[policy.id] = policy.modified_at;
                        }
                        localStorage.n_policy_accept_times = JSON.stringify(policyAcceptTimes);
                    }
                }
            ])
        });
    }

    logTarget(target: string) {
        this.currentTarget = target;
        this.log.pushLog({msg: '[STARTUP] Reaching (' + target + ')', type: 'info'});
    }
}
