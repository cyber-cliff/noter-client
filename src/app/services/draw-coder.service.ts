import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DrawCoderService {
  constructor() { }

  codeDrawData(drawArray) {
    if (drawArray.length === 0) {
      return '';
    }
    let baseTime = 0; // The t123 will then stand for baseTime + 123

    for (let entryArray of drawArray) {
      for (let entry of entryArray) {
        entry.x = Math.round(entry.x);
        entry.y = Math.round(entry.y);
        entry.color = 0;
        if (!baseTime || entry.time < baseTime) {
          baseTime = entry.time;
        }
      }
    }

    let miniArray = [];

    for (let entryArray of drawArray) {
      let miniEntryArray = [];
      for (let entry of entryArray) {
        miniEntryArray.push(`x${entry.x}y${entry.y}t${entry.time - baseTime}c${entry.color}`);
      }
      miniArray.push(miniEntryArray);
    }

    let final = miniArray.join('/'); // '/' is dividing array groups
    final = 'b' + baseTime + '/' + final;

    return final;
  }

  decodeDrawData(codedData) {
    const drawArray = codedData.split('/');

    const baseTimeKVString = drawArray.shift();
    const baseTime = parseInt(baseTimeKVString.slice(1), 10);

    const newDrawArray = [];
    for (const entryArray of drawArray) {
      newDrawArray.push(entryArray.split(','));
    }

    const finalDrawArray = [];
    for (const entryArray of newDrawArray) {
      const finalEntryArray = [];
      for (const entry of entryArray) {
        let values = {};
        let currentKeyName = '';
        let currentValueBuffer = '';
        for (const char of entry) {
          // Either parse '1'-'9' or if it's '0', accept it too since without the extra condition it would be false otherwise
          if (parseInt(char) || char === '0') {
            // If it's a number
            currentValueBuffer += char;
          } else {
            if (currentKeyName) { // If we finished getting values and are on the next letter (key) we insert the key-value pair into values
              values[currentKeyName] = parseInt(currentValueBuffer);
              currentValueBuffer = '';
            }

            switch (char) {
              case 'x':
                currentKeyName = 'x';
                break;
              case 'y':
                currentKeyName = 'y';
                break;
              case 't':
                currentKeyName = 'time';
                break;
              case 'c':
                currentKeyName = 'color';
                break;
            }
          }
        }

        if (currentKeyName) { // One more time since there is no last letter character to trigger it
          values[currentKeyName] = parseInt(currentValueBuffer, 10);
          currentValueBuffer = '';
        }

        finalEntryArray.push(values);
      }

      finalDrawArray.push(finalEntryArray);
    }

    // Replace colors plz
    for (let entryArray of finalDrawArray) {
      for (let entry of entryArray) {
        entry.time += baseTime;
        entry.color = '#fff';
      }
    }

    return finalDrawArray;
  }
}
