import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class StateService {

    noteSaved = true;

    apiFetching = {
        fetching: false,
        direction: undefined,
        endpoint: undefined
    };

    splashScreenText = ' ';

    afterUpdate = false; // If the client had been just updated

    constructor() {
    }
}
