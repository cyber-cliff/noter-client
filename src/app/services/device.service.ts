import { Injectable } from '@angular/core';
import { LogService } from './log.service';

@Injectable({
  providedIn: 'root',
})
export class DeviceService {
  isSoftKeyboardOpen = false;

  prevWidth = window.innerWidth;
  prevHeight = window.innerHeight;

  constructor(private log: LogService) {
    window.addEventListener('resize', (event) => {
      this.checkSoftKeyboardStatus(event);
    });
  }

  checkSoftKeyboardStatus(event: any) {
    const deltaWidth = window.innerWidth - this.prevWidth;
    const deltaHeight = window.innerHeight - this.prevHeight;

    this.prevWidth = window.innerWidth;
    this.prevHeight = window.innerHeight;

    if (deltaWidth === 0 && deltaHeight < -300) {
      this.isSoftKeyboardOpen = true;
      this.log.pushLog({
        type: 'success',
        msg: `[DEVICE] Soft keyboard open detected dy:${deltaHeight}`
      });
    } else {
      if (this.isSoftKeyboardOpen && deltaHeight < 75) { return; } // Keep the open status if it's already true for some tolerance

      if (this.isSoftKeyboardOpen) {
        this.log.pushLog({
          type: 'success',
          msg: `[DEVICE] Soft keyboard close detected dy:${deltaHeight}`
        });
      }
      this.isSoftKeyboardOpen = false;
    }
  }
}
