import {Injectable} from '@angular/core';
import {NoteThumb} from '../types/noteThumb';

@Injectable({
    providedIn: 'root'
})
export class ActivityService {

    constructor() {
    }

    storeNoteOpen(noteThumb: NoteThumb, tags: Array<any>) {
        this.checkStorages();
        if (!localStorage.n_recent_opens) {
            localStorage.n_recent_opens = '[]';
        }

        // NoteData = noteThumb + tags
        const noteData = {
            noteThumb,
            tags
        };

        let recentOpens = JSON.parse(localStorage.n_recent_opens);

        // Get rid of that note thumb if it is stored
        recentOpens = recentOpens.filter(storedNoteData => {
            return noteThumb.id !== storedNoteData.noteThumb.id;
        });

        while (recentOpens.length > 6 - 1) {
            recentOpens.shift();
        }

        recentOpens.push(noteData);

        localStorage.n_recent_opens = JSON.stringify(recentOpens);
    }

    getRecentlyOpenedNotes() {
        this.checkStorages();
        if (!localStorage.n_recent_opens) {
            localStorage.n_recent_opens = '[]';
        }

        const recentlyOpenedNotes = JSON.parse(localStorage.n_recent_opens).reverse();

        return recentlyOpenedNotes;
    }

    checkStorages() {
        try {
            const recentlyOpenedNotes = JSON.parse(localStorage.n_recent_opens).reverse();
            for (const noteData of recentlyOpenedNotes) {
                if (!noteData.noteThumb || !noteData.tags) {
                    throw new Error(('No noteThumb or tags array present'));
                }
            }
        } catch (e) {
            console.error('Storage check failed!', e);
            console.log('Storage dump: ', localStorage.n_recent_opens);
            localStorage.removeItem('n_recent_opens');
            return false;
        }
    }
}
