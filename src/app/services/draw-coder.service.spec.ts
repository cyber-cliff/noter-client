import { TestBed } from '@angular/core/testing';

import { DrawCoderService } from './draw-coder.service';

describe('DrawCoderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DrawCoderService = TestBed.get(DrawCoderService);
    expect(service).toBeTruthy();
  });
});
