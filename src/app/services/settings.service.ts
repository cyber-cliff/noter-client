import { Injectable } from '@angular/core';
import { EventEmitterService } from '../event-emitter.service';
import { ThemeService } from '../theme/theme.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {StateService} from './state.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  noterVersion = {
    code: 20052501, // xxyyzzaa - xx-year - yy-month - zz-day - aa-count(starts @ 01) - everything 2 digit
    text: '',
    phase: 'alpha',
    resetUnderVersion: 20021101
  };

  values = {
    version: 0,

    localization_language: 'en',

    startup_splash_delay: 'fast', // ultra, [fast], wait

    appearance_theme: 'dark', // dark, light
    appearance_app_background: 'plain', // plain, apod
    appearance_blurs: false,
    appearance_accent_color: 'var(--color-lblue)', // [var(--color-lblue)] any valid css color
    appearance_show_scrollbars: false, // [false], true
    appearance_transition_duration: 0.15, // [0.15] any float

    editor_spellchecker: false,

    privacy_diag: true,
    privacy_active_time: true,

    debug_enabled: false,
  };

  isPWA = false;

  constructor(
    private eventEmitter: EventEmitterService,
    private theme: ThemeService,
    private router: Router,
    private translate: TranslateService,
    private state: StateService
  ) {
    this.makeVersionString();
  }

  saveSettings() {
    localStorage.n_settings = JSON.stringify(this.values);
    this.updateManualSettings();
  }

  loadSettings() {
    if (!localStorage.n_settings) { this.saveSettings(); return; }

    let storedSettings;
    try {
      storedSettings = JSON.parse(localStorage.n_settings);
    } catch (e) {
      // Settings corrupt!

      setTimeout(() => {
        this.eventEmitter.pushNotif({
          icon: 'fas fa-exclamation-triangle',
          details: `Settings corrupt! Restoring defaults. If this keeps happening, open an issue on \`noter-client\` GitLab repo.`,
          color: 'var(--color-orange)',
          duration: 0,
        });
      }, 500);

      this.saveSettings();
      return;
    }

    for (let settingKey of Object.keys(storedSettings)) {
      this.values[settingKey] = storedSettings[settingKey];
    }

    if (this.values.version === 0 || !this.values.version) {
      // New install
      console.log('[VERSION] No noter version stored. Creating one.');

      this.values.version = this.noterVersion.code;
      this.saveSettings();
    } else if (
      this.values.version < this.noterVersion.code ||
      localStorage.getItem('n_display_underversion_notif') // This is because we refresh after underversion reset...
    ) {
      // Just updated
      console.log('[VERSION] Stored noter version less than updated. Triggering after-update process.');

      setTimeout(() => {
        this.state.afterUpdate = true;

        if (this.values.version < this.noterVersion.resetUnderVersion) { // If under this version, delete stored data
          localStorage.clear();
          sessionStorage.clear();

          this.loadSettings();
          this.values.version = this.noterVersion.code;
          this.saveSettings();

          console.log('Under versioned. Deleted storages.');

          localStorage.n_display_underversion_notif = 'true';

          location.reload();
        }

        this.values.version = this.noterVersion.code;
        this.saveSettings();
      }, 500);
    }

    this.updateManualSettings();
  }

  updateManualSettings() { // For methods that need to be called on settings update
    this.theme.setThemeFromString(this.values.appearance_theme, this.values.appearance_app_background);
    this.theme.setAccentColor(this.values.appearance_accent_color);
    this.theme.setTransitionDuration(this.values.appearance_transition_duration);
    this.theme.setBlurFilter(this.values.appearance_blurs);
    this.translate.use(this.values.localization_language);
  }

  makeVersionString() {
    const vcstr = '' + this.noterVersion.code;

    const year = parseInt(vcstr[0] + vcstr[1], 10);
    const month = parseInt(vcstr[2] + vcstr[3], 10);
    const day = parseInt(vcstr[4] + vcstr[5], 10);
    const patch = parseInt(vcstr[6] + vcstr[7], 10);

    const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');

    this.noterVersion.text = `${this.noterVersion.phase}${year}.${month}.${day}${(patch > 1) ? alphabet[patch - 1] : ''}`;
  }
}
