import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class MetaService {

  defaultTags = {
    site: 'Noter',
    title: 'Noter',
    description: 'Free* note writing, sharing, sorting app.'
  };

  constructor(private meta: Meta) { }

  updateTags({ site, title, description }: { site?: string; title?: string; description?: string; } = {}) {
    if (site) {
      this.meta.updateTag({property: 'og:site_name', content: site});
    }
    if (title) {
      this.meta.updateTag({name: 'title', content: title});
      this.meta.updateTag({property: 'og:title', content: title});
      this.meta.updateTag({property: 'twitter:title', content: title});
    }
    if (description) {
      this.meta.updateTag({name: 'description', content: description});
      this.meta.updateTag({property: 'og:description', content: description});
      this.meta.updateTag({property: 'twitter:description', content: description});
    }
  }

  restoreDefault() {
    this.updateTags(this.defaultTags);
  }


}
