import { TestBed } from '@angular/core/testing';

import { NoteStorageService } from './note-storage.service';

describe('NoteStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NoteStorageService = TestBed.get(NoteStorageService);
    expect(service).toBeTruthy();
  });
});
