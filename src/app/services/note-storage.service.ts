import { Injectable } from '@angular/core';
import { EventEmitterService } from '../event-emitter.service';

@Injectable({
  providedIn: 'root'
})
export class NoteStorageService {

  storedNotes = [];

  constructor(private eventEmitter: EventEmitterService) { }

  loadStore() {
    if (!localStorage.n_store) { return; }

    this.storedNotes = JSON.parse(localStorage.n_store);

    this.storedNotes = this.storedNotes.filter((storedNoteData) => { // Filter out bad notes
      return storedNoteData.note &&
      storedNoteData.tags &&
      storedNoteData.users &&
      storedNoteData.storeLastUpdate;
    });
  }

  saveStore() {
    localStorage.n_store = JSON.stringify(this.storedNotes);
  }

  hasNoteData(noteId) {
    return !!this.storedNotes.filter(storedNoteData => storedNoteData.note.id === noteId)[0];
  }

  storeNote(noteData) {
    noteData.storeLastUpdate = new Date().getTime();

    this.storedNotes.push(noteData);
    this.saveStore();
  }

  updateStoredNote(noteData) {
    let noteDataToUpdate = this.storedNotes.filter(storedNoteData => storedNoteData.note.id === noteData.note.id)[0];

    noteDataToUpdate.storeLastUpdate = new Date().getTime();

    noteDataToUpdate = noteData;

    this.saveStore();
  }


  getStoredNoteData(noteId) {
    const noteData = this.storedNotes.filter(storedNoteData => storedNoteData.note.id === noteId)[0];

    return noteData;
  }

  deleteNoteDataFromStorage(noteId) {
    this.storedNotes = this.storedNotes.filter(storedNoteData => storedNoteData.note.id !== noteId);
    this.saveStore();
  }
}
