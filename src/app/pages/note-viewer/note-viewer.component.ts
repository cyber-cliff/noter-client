import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from 'src/app/api.service';
import {OverlayService} from 'src/app/shell/overlays/overlay.service';
import {Title} from '@angular/platform-browser';
import {MetaService} from 'src/app/services/meta.service';
import {async} from 'rxjs/internal/scheduler/async';
import {NoteStorageService} from 'src/app/services/note-storage.service';
import {EventEmitterService} from 'src/app/event-emitter.service';
import {ActivityService} from '../../services/activity.service';

@Component({
    selector: 'app-note-viewer',
    templateUrl: './note-viewer.component.html',
    host: {
        '(window:keydown)': 'hotkeys($event)',
        '(window:keyup)': 'keyUp($event)',
    },
    styleUrls: ['./note-viewer.component.scss']
})
export class NoteViewerComponent implements OnInit {

    noteId: string;

    fetching = false;
    res: any;

    hideToolbar = false;
    prevPagePos = 0;

    simpleMDEs = [];

    index = [];

    createdDelta = 'N/A';
    modifiedDelta = 'N/A';

    fromSaved = false; // TODO: rename!

    hotkeysData = [
        {
            code: 'KeyI', handler: () => {
                this.openIndexOverlay();
            }
        },
        {
            code: 'KeyT', handler: () => {
                this.scrollToTop();
            }
        },
        {
            code: 'KeyS', handler: () => {
                this.openSaveNoteOverlay();
            }
        },
        {
            code: 'KeyP', handler: () => {
                this.openProps();
            }
        },
        {
            code: 'KeyE', handler: () => {
                this.router.navigate(['/n/' + this.noteId + '/edit']);
            }
        },
        {
            code: 'struct', canInput: false, ctrl: false, alt: false, shift: false, handler: () => {
                this.openIndexOverlay();
            }
        }
    ];

    @ViewChild('c') cElement: ElementRef;

    constructor(
        private route: ActivatedRoute,
        private overlayService: OverlayService,
        private title: Title,
        private meta: MetaService,
        private noteStore: NoteStorageService,
        private eventEmitter: EventEmitterService,
        private router: Router,
        private activityService: ActivityService,
        public api: ApiService
    ) {
        this.route.params.subscribe(params => {
            this.noteId = params.noteId;
            this.fetchNote();

            if (!document.querySelector('.page-wrapper')) {
                return;
            }
            document.querySelector('.page-wrapper').addEventListener('scroll', (event) => {
                if (event.target['scrollTop'] > this.prevPagePos) {
                    this.hideToolbar = true;
                } else {
                    this.hideToolbar = false;
                }
                this.prevPagePos = event.target['scrollTop'];
            });
        });
    }

    ngOnInit() {
    }

    async fetchNote() {
        this.res = undefined;
        this.fetching = true;
        this.res = await this.api.request('notes/get', {note_id: this.noteId}, true);
        this.fetching = false;
        if (this.res.success) {
            // We do this only if the note came from the API!
            if (this.noteStore.hasNoteData(this.noteId)) {
                this.noteStore.updateStoredNote(this.res.data);
            }

            this.postFetch();
        } else {
            // Fetch failed so let's try to get it from the storage!
            const storedNoteData = this.noteStore.getStoredNoteData(this.noteId);
            if (!storedNoteData) {
                return false;
            }

            this.res.success = true;
            this.res.data = storedNoteData; // We 'fake' a response from the api with the stored res from when it was stored
            this.fromSaved = true; // TODO: rename!

            this.postFetch();
        }
    }

    async postFetch() {
        const ndfVer = this.res.data.note.doc.head.ndfVersion;
        if (![4, 5].includes(ndfVer)) {
            this.eventEmitter.pushNotif({
                icon: 'fas fa-times-circle',
                // tslint:disable-next-line:max-line-length
                details: `This note uses unsupported NDF version and therefore cannot be displayed. If this is an offline note, please refresh this page with working internet connection - that should automatically update everything.`,
                color: 'var(--color-red)',
                duration: 0
            });
            this.res = {
                success: false,
                error: {formatError: true}
            };

            return;
        }

        // We don't want the doc stored since we need noteThumb only
        const noteThumb = JSON.parse(JSON.stringify(this.res.data.note));
        noteThumb.type = noteThumb.doc.head.type || 'text'; // TODO: remove when #29 on noter-server is closed
        noteThumb.doc = undefined;
        this.activityService.storeNoteOpen(noteThumb, this.res.data.tags);

        const encryption = this.res.data.note.doc.head.encryption;
        if (encryption) {
            if (encryption.type !== 'AES_JSON') {
                this.eventEmitter.pushNotif({
                    icon: 'fas fa-times-circle',
                    // tslint:disable-next-line:max-line-length
                    details: `This note uses unsupported encryption format.`,
                    color: 'var(--color-red)',
                    duration: 0
                });
                this.res = {
                    success: false,
                    error: {encryptionError: true}
                };

                return;
            }

            // Let's decrypt the note now!

            const decryptRes = await this.overlayService.showOverlay('encrypt-password-prompt', {
                type: encryption.type,
                encryptedData: this.res.data.note.doc.body
            });

            console.log(decryptRes);

            if (!decryptRes.success) {
                this.res = {
                    success: false,
                    error: {encryptionError: true}
                };

                return;
            }

            this.res.data.note.doc.body = decryptRes.decryptedData;
        }

        this.title.setTitle(`${this.res.data.note.title} | Noter`);

        this.createdDelta = this.calcDateDeltaString(this.res.data.note.created_at);
        this.modifiedDelta = this.calcDateDeltaString(this.res.data.note.modified_at);

        setTimeout(() => {
            this.meta.updateTags({
                title: this.res.data.note.title,
                description: this.cElement.nativeElement.innerText.slice(0, 255) + '...'
            });

            const headings = this.cElement.nativeElement.querySelectorAll(
                'h1,h2,h3,h4,h5,h6'
            );
            console.log(headings);

            let ids = [];
            for (const heading of headings) {
                const headingLevel = parseInt(heading.tagName.slice(1), 10);

                ids.push({
                    id: heading.id,
                    title: heading.innerText,
                    level: headingLevel,
                    el: heading
                });
            }

            this.index = ids;
        }, 1);
    }


    async openProps() {
        const editedSomething = await this.overlayService.showOverlay('prop-edit', this.res.data.note);
        if (editedSomething) {
            this.fetchNote();
        }
    }

    scrollToTop() {
        document.querySelector('.page-wrapper').scrollTop = 0;
    }

    openIndexOverlay() {
        this.overlayService.showOverlay('note-index', this.index);
    }

    openSaveNoteOverlay() {
        this.overlayService.showOverlay('note-save', this.res.data);
    }

    calcDateDeltaString(time: number) {
        //if (!time || time === 0) { return 'N/A'; }
        if (time <= 1576164788708) {
            return '∞ days ago';
        }
        time = new Date().getTime() - time; // Get delta

        const minutes = time / 60 / 1000;
        const hours = minutes / 60;
        const days = hours / 24;

        let final;
        if (Math.floor(minutes) > 1) {
            final = Math.floor(minutes) + ' minutes ago';
        }
        if (Math.floor(minutes) === 1) {
            final = Math.floor(minutes) + ' minute ago';
        }

        if (Math.floor(hours) > 1) {
            final = Math.floor(hours) + ' hours ago';
        }
        if (Math.floor(hours) === 1) {
            final = Math.floor(hours) + ' hour ago';
        }

        if (Math.floor(days) > 1) {
            final = Math.floor(days) + ' days ago';
        }
        if (Math.floor(days) === 1) {
            final = Math.floor(days) + ' day ago';
        }

        if (!final) {
            final = 'a few seconds ago';
        }

        return final;
    }

    hotkeys(event) {
        // if (this.wasKeyDown) { return; }

        for (const hotkey of this.hotkeysData) {
            if (
                event.code === hotkey.code &&
                event.ctrlKey === !!hotkey.ctrl &&
                event.altKey === !!hotkey.alt &&
                event.shiftKey === !!hotkey.shift
            ) {
                if (event.srcElement.localName === 'input' && !(hotkey.canInput || !!hotkey.ctrl || !!hotkey.alt)) {
                    return;
                }
                if (!!event.srcElement.attributes.contenteditable && !(hotkey.canInput || !!hotkey.ctrl || !!hotkey.alt)) {
                    return;
                }

                event.preventDefault();

                hotkey.handler();
                return false;
            }
        }
    }

    keyUp(event) {
    }

}
