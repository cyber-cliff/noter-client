import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { EventEmitterService } from 'src/app/event-emitter.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  fetching = false;
  res: {
    success: boolean,
    data?: any,
    error?: any
  };

  newTask = {
    name: '',
    dateString: '',
    timestamp: undefined
  };

  dateGroups = [];

  constructor(
    private api: ApiService
  ) { }

  ngOnInit(): void {
    this.fetchTasks();
  }

  async fetchTasks() {
    this.fetching = true;

    this.res = await this.api.request('tasks/get', {}, true);

    if (this.res.success) {
      this.createDateGroups();
    }

    this.fetching = false;
  }

  async sendNewTask() {
    if (!this.newTask.name || !this.newTask.dateString) { return; }
    this.newTask.timestamp = new Date(this.newTask.dateString).getTime();

    console.log(this.newTask);

    const res = await this.api.request('tasks/create', this.newTask, true);

    if (!res.success) {
      return;
    }

    this.newTask = {
      name: '',
      dateString: '',
      timestamp: undefined
    };

    this.fetchTasks();
  }

  createDateGroups() {
    this.dateGroups = [];

    this.res.data.tasks = this.res.data.tasks.sort((a, b) => {
      return a.timestamp - b.timestamp;
    });

    for (const task of this.res.data.tasks) {
      const dateGroup = this.dateGroups.filter(compDateGroup => {
        return compDateGroup.dateString === new Date(task.timestamp).toLocaleDateString();
      })[0];


      if (!dateGroup) {
        let timeTag = 'none';

        const groupStartTimestamp = new Date(
          new Date(task.timestamp).toLocaleDateString()
        ).getTime();

        const todayStartTimestamp = new Date(
          new Date().toLocaleDateString()
        ).getTime();

        if (groupStartTimestamp < todayStartTimestamp) {
          timeTag = 'overdue';
        }
        if (groupStartTimestamp === todayStartTimestamp) {
          timeTag = 'today';
        }

        this.dateGroups.push({
          dateString: new Date(task.timestamp).toLocaleDateString(),
          timeTag,
          tasks: [task]
        });
      } else {
        dateGroup.tasks.push(task);
      }
    }
  }

}
