import { Component, OnInit } from '@angular/core';
import { NoteStorageService } from 'src/app/services/note-storage.service';

@Component({
  selector: 'app-downloaded-notes',
  templateUrl: './downloaded-notes.component.html',
  styleUrls: ['./downloaded-notes.component.scss']
})
export class DownloadedNotesComponent implements OnInit {
  data = [];

  constructor(private noteStore: NoteStorageService) { }

  ngOnInit() {
    this.fetchNotes();
  }

  fetchNotes() {
    this.data = this.noteStore.storedNotes;
    this.data = this.data.sort((a, b) => {
      return (b.storeLastUpdate - a.storeLastUpdate);
    });
  }

}
