import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadedNotesComponent } from './downloaded-notes.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('DownloadedNotesComponent', () => {
  let component: DownloadedNotesComponent;
  let fixture: ComponentFixture<DownloadedNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [ DownloadedNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadedNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
