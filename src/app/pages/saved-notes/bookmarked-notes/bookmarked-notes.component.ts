import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Subscription } from 'rxjs';
import { EventEmitterService } from 'src/app/event-emitter.service';

@Component({
  selector: 'app-bookmarked-notes',
  templateUrl: './bookmarked-notes.component.html',
  styleUrls: ['./bookmarked-notes.component.scss']
})
export class BookmarkedNotesComponent implements OnInit {
  res: any;
  fetching = false;

  loginStatusChangeSub: Subscription;

  constructor(
    private eventEmitter: EventEmitterService,
    public api: ApiService
  ) { }

  ngOnInit() {
    this.fetchBookmarkedNotes();

    this.loginStatusChangeSub = this.eventEmitter.loginStatusChangeEmitter.subscribe(data => {
      if (data.loggedIn) {
        this.fetchBookmarkedNotes();
      }
    });
  }

  ngOnDestroy() {
    this.loginStatusChangeSub.unsubscribe();
  }

  async fetchBookmarkedNotes() {
    this.fetching = true;

    this.res = await this.api.request('users/saved/get', {}, true);

    this.fetching = false;
  }

}
