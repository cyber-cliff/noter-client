import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarkedNotesComponent } from './bookmarked-notes.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('BookmarkedNotesComponent', () => {
  let component: BookmarkedNotesComponent;
  let fixture: ComponentFixture<BookmarkedNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [ BookmarkedNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkedNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
