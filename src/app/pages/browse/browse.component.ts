import {Component, OnInit, ViewChild, ElementRef, AfterContentInit, AfterViewInit} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { OverlayService } from 'src/app/shell/overlays/overlay.service';
import { ApiService } from 'src/app/api.service';
import {SettingsService} from '../../services/settings.service';
import {StateService} from '../../services/state.service';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.scss']
})
export class BrowseComponent implements OnInit {

  // * default vars
  publishedNotes = [];
  publishedNotesTags = [];
  publishedNotesFetching = false;

  newsNotes = [];
  newsNotesTags = [];
  newsNotesFetching = false;

  constructor(
    private title: Title,
    public overlayService: OverlayService,
    public api: ApiService,
    public settings: SettingsService,
    public state: StateService
  ) {
  }

  ngOnInit() {
    this.title.setTitle(`Browse | Noter`);
    this.fetchPublishedNotes();
    this.fetchNewsNotes();
  }

  async fetchPublishedNotes() {
    this.publishedNotesFetching = true;
    const res = await this.api.request('discover/published', {}, true);
    this.publishedNotes = (res.success) ? res.data.notes : [];
    this.publishedNotesTags = (res.success) ? res.data.tags : [];

    this.publishedNotesFetching = false;
  }

  async fetchNewsNotes() {
    this.newsNotesFetching = true;
    const res = await this.api.request('discover/news', {}, true);
    this.newsNotes = (res.success) ? res.data.notes : [];
    this.newsNotesTags = (res.success) ? res.data.tags : [];

    for (const note of this.newsNotes) {
      note.date = new Date(note.modified_at).toLocaleDateString();
    }

    this.newsNotesFetching = false;
  }
}
