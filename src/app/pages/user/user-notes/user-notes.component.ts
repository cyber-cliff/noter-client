import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { SettingsService } from 'src/app/services/settings.service';
import { Title } from '@angular/platform-browser';
import { OverlayService } from 'src/app/shell/overlays/overlay.service';
import { Subscription } from 'rxjs';
import { EventEmitterService } from 'src/app/event-emitter.service';

@Component({
  selector: 'app-user-notes',
  templateUrl: './user-notes.component.html',
  styleUrls: ['./user-notes.component.scss']
})
export class UserNotesComponent implements OnInit {

  userId: string;
  data: any;

  fetchFinished = false;
  fetchFailed = false;

  currentTime = new Date().getTime();

  sortingMode = {
    type: 'modified_at',
    dir: 'asc'
  };
  groupingMode = 'tags';

  tagGroups = [];
  accessGroups:any = {};

  prevPagePos = 0;
  hideToolbar = false;

  loginStatusChangeSub: Subscription;

  createdAtDelta = 'N/A';
  activeAtDelta = 'N/A';

  constructor(
    private route: ActivatedRoute,
    private title: Title,
    private overlayService: OverlayService,
    private ref: ChangeDetectorRef,
    private eventEmitter: EventEmitterService,
    public api: ApiService,
    public settings: SettingsService
  ) {
    if (!document.querySelector('.page-wrapper')) { return; }
    document.querySelector('.page-wrapper').addEventListener('scroll', (event) => {
      if (event.target['scrollTop'] > this.prevPagePos) {
        this.hideToolbar = true;
      } else {
        this.hideToolbar = false;
      }
      this.prevPagePos = event.target['scrollTop'];
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userId = params.userId;
      this.fetchNotes();
    });

    this.loginStatusChangeSub = this.eventEmitter.loginStatusChangeEmitter.subscribe(data => {
      if (this.api.loggingIn) { return; }
      if (data.loggedIn) {
        this.fetchNotes();
      }
    });
  }

  ngOnDestroy() {
    this.loginStatusChangeSub.unsubscribe();
  }

  async fetchNotes() {
    this.data = false;
    this.fetchFinished = false;
    this.tagGroups = [];

    try {
      const res = await this.api.request('users/get', { user_id: this.userId }, true);
      if (!res.success) {
        throw ('no success');
      }
      this.data = res.data;
      this.fetchFinished = true;
    } catch (e) {
      this.fetchFailed = true;
      this.fetchFinished = true;
      return;
    }

    if (this.data) {
      this.title.setTitle(`${this.data.profile.username}'s notes | Noter`);
      this.createTagGroups();
      this.createAccessibilityGroups();
      this.createdAtDelta = this.calcDateDeltaString(this.data.profile.created_at);
      this.activeAtDelta = this.calcDateDeltaString(this.data.profile.active_at);
    }
  }

  async noteUpdate() {
    await this.fetchNotes();
  }

  createTagGroups() {
    if (!this.data.notes) { return false; }

    if (this.data.tags) {
      for (const tag of this.data.tags) {
        const notes = this.data.notes.filter((note) => {
          if (!note.tags) { return false; }
          return note.tags.includes(tag.id);
        });

        this.tagGroups.push({
          tag,
          notes
        });
      }
    }

    const untaggedNotes = this.data.notes.filter((note) => {
      if (!note.tags || note.tags.length < 1) { return true; }
    });

    if (untaggedNotes.length > 0) { // If any untagged notes are present
      this.tagGroups.push({ // Create a "No tags" category with them, so they don't feel alone
        tag: {
          name: 'No tags',
          color: 'var(--color-text)'
        },
        notes: untaggedNotes
      });
    }
  }

  createAccessibilityGroups() {
    if (!this.data.notes) { return false; }

    const privateNotes = [];
    const invisibleNotes = [];
    const nonDiscoverableNotes = [];
    const publishedNotes = [];

    for (const note of this.data.notes) {
      if (note.access === 0) { privateNotes.push(note); continue; }
      if (note.access === 1) { invisibleNotes.push(note); continue; }
      if (note.access === 3) { publishedNotes.push(note); continue; }
      nonDiscoverableNotes.push(note);
    }

    this.accessGroups = {
      private: privateNotes,
      invisible: invisibleNotes,
      nonDiscoverable: nonDiscoverableNotes,
      published: publishedNotes
    };
  }

  openNewNoteOverlay() {
    this.overlayService.showOverlay('new-note');
  }

  calcDateDeltaString(time: number) {
    //if (!time || time === 0) { return 'N/A'; }
    if (time <= 1576164788708) { return '∞ days ago'; }
    time = new Date().getTime() - time; // Get delta

    const minutes = time / 60 / 1000;
    const hours = minutes / 60;
    const days = hours / 24;

    let final;
    if (Math.floor(minutes) > 1) { final = Math.floor(minutes) + ' minutes ago'; }
    if (Math.floor(minutes) === 1) { final = Math.floor(minutes) + ' minute ago'; }

    if (Math.floor(hours) > 1) { final = Math.floor(hours) + ' hours ago'; }
    if (Math.floor(hours) === 1) { final = Math.floor(hours) + ' hour ago'; }

    if (Math.floor(days) > 1) { final = Math.floor(days) + ' days ago'; }
    if (Math.floor(days) === 1) { final = Math.floor(days) + ' day ago'; }

    if (!final) { final = 'a few seconds ago'; }

    return final;
  }

  barChange({sortingMode, groupingMode}) {
    console.log('unp-barChange', sortingMode, groupingMode)
    this.sortingMode = sortingMode;
    this.groupingMode = groupingMode;
  }
}
