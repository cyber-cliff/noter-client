import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OverlayService } from 'src/app/shell/overlays/overlay.service';

@Component({
  selector: 'app-filter-bar',
  templateUrl: './filter-bar.component.html',
  styleUrls: ['./filter-bar.component.scss']
})
export class FilterBarComponent implements OnInit {

  @Input() sortingMode: any;
  @Input() groupingMode: any;

  @Output('change')
  changeEvent = new EventEmitter<any>();

  constructor(private overlayService: OverlayService) { }

  ngOnInit() {
  }

  async openListGroupingOverlay() {
    const res = await this.overlayService.showOverlay('list-grouping', this.groupingMode);
    console.log('grouping overlay res', res);
    if (res) {
      this.groupingMode = res;

      // Now returned changed values

      this.changeEvent.emit({
        sortingMode: this.sortingMode,
        groupingMode: this.groupingMode
      });
    }
  }

}
