import {Component, OnInit, HostListener, OnDestroy} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { EventEmitterService } from 'src/app/event-emitter.service';
import { OverlayService } from 'src/app/shell/overlays/overlay.service';
import { Observable } from 'rxjs';
import { StateService } from 'src/app/services/state.service';

import * as CryptoJS from 'crypto-js';
import {ActivityService} from '../../services/activity.service';

@Component({
  selector: 'app-note-editor',
  templateUrl: './note-editor.component.html',
  host: {
    '(window:keydown)': 'hotkeys($event)'
  },
  styleUrls: ['./note-editor.component.scss']
})
export class NoteEditorComponent implements OnInit, OnDestroy {

  noteId: string;

  fetching = false;
  res: any;

  saveFetching = false;
  saveFetchRes: any;
  saved = true;
  canSave = true; // To not DoS the API

  hideToolbar = false;
  prevPagePos = 0;

  lengthLimit = 100000;
  percUsed = 0;

  autosaveInterval;
  encryptionPassword = false;

  hotkeysData = [
    {
      code: 'KeyS', ctrl: true, canInput: true, handler: () => {
        this.save();
      }
    },
    {
      code: 'KeyP', ctrl: true, canInput: true, handler: () => {
        this.openProps();
      }
    },

    {
      key: 'struct', code: 'struct', canInput: false, ctrl: false, alt: false, shift: false, handler: () => {
        //
      }
    }
  ];

  @HostListener('window:beforeunload')
  canDeactivate(): Observable<boolean> | boolean {
    if (!this.res.data) { return true; }

    return this.saved;
  }

  constructor(
    private route: ActivatedRoute,
    private eventEmitter: EventEmitterService,
    private overlayService: OverlayService,
    private router: Router,
    private state: StateService,
    private activityService: ActivityService,
    public api: ApiService
  ) {
    this.route.params.subscribe(params => {
      this.noteId = params.noteId;
      this.fetchNote();

      if (!document.querySelector('.page-wrapper')) { return; }
      document.querySelector('.page-wrapper').addEventListener('scroll', (event) => {
        if (event.target['scrollTop'] > this.prevPagePos) {
          this.hideToolbar = true;
        } else {
          this.hideToolbar = false;
        }
        this.prevPagePos = event.target['scrollTop'];
      });
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    clearInterval(this.autosaveInterval);
    this.state.noteSaved = true;
  }

  async fetchNote() {
    this.res = undefined;
    this.encryptionPassword = false;
    this.fetching = true;
    this.res = await this.api.request('notes/get', { note_id: this.noteId }, true);

    const encryption = this.res.data.note.doc.head.encryption;
    if (encryption) {
      if (encryption.type !== 'AES_JSON') {
        this.eventEmitter.pushNotif({
          icon: 'fas fa-times-circle',
          // tslint:disable-next-line:max-line-length
          details: `This note uses unsupported encryption format.`,
          color: 'var(--color-red)',
          duration: 0
        });
        this.res = {
          success: false,
          error: {encryptionError: true}
        };

        return;
      }

      // Let's decrypt the note now!

      const decryptRes = await this.overlayService.showOverlay('encrypt-password-prompt', {
        type: encryption.type,
        encryptedData: this.res.data.note.doc.body
      });

      console.log(decryptRes);

      if (!decryptRes.success) {
        this.res = {
          success: false,
          error: {encryptionError: true}
        };

        return;
      }

      this.res.data.note.doc.body = decryptRes.decryptedData;
      this.encryptionPassword = decryptRes.encryptionPassword;
    }

    // We don't want the doc stored since we need noteThumb only
    const noteThumb = JSON.parse(JSON.stringify(this.res.data.note));
    noteThumb.type = noteThumb.doc.head.type || 'text'; // TODO: remove when #29 on noter-server is closed
    noteThumb.doc = undefined;
    this.activityService.storeNoteOpen(noteThumb, this.res.data.tags);


    this.fetching = false;

    if (this.autosaveInterval) { clearInterval(this.autosaveInterval); }
    this.autosaveInterval = setInterval(() => {
      this.save();
    }, 60000);

    this.state.noteSaved = true;

    this.calculatePercUsed();
  }

  async save() {
    if (!this.canSave || this.saved) {
      return;
    }

    this.canSave = false;
    this.saveFetching = true;

    const doc = JSON.parse(JSON.stringify(this.res.data.note.doc));
    if (this.encryptionPassword) {
      doc.body = CryptoJS.AES.encrypt(JSON.stringify(doc.body), this.encryptionPassword).toString();
    }

    const res = await this.api.request('notes/update', {
      note_id: this.noteId,
      note: {
        doc // just save the doc, nothing else
      }
    }, true);

    this.saveFetching = false;
    this.saveFetchRes = res;

    setTimeout(() => {
      this.canSave = true;
    }, 2500);

    if (res.success) {
      this.saved = true;
      this.state.noteSaved = true;
    } else if (res.error) {
      if (!res.error.fetchError) {
        if (res.error.code === 9) {
          this.eventEmitter.pushNotif({
            icon: 'fas fa-save',
            details: 'Cannot save! Note is over the size limit :/',
            color: 'var(--color-red)'
          });
          return;
        }
      }

      if (!res.error.fetchError) {
        this.eventEmitter.pushNotif({
          icon: 'fas fa-save',
          details: 'Failed to save note',
          color: 'var(--color-red)'
        });
      }
    }
  }

  async openProps() {
    const editedSomething = await this.overlayService.showOverlay('prop-edit', this.res.data.note);
    if (editedSomething) {
      this.fetchNote();
    }
  }

  invalidateSavedState() {
    this.saved = false;
    this.state.noteSaved = false;
  }

  addSection(sectionType: string) {
    this.invalidateSavedState();

    switch (sectionType) {
      case 'markdown':
        this.res.data.note.doc.body.sections.push({
          type: 'markdown',
          content: '# Write markdown here!'
        });
        break;
      case 'draw':
        this.res.data.note.doc.body.sections.push({
          type: 'draw',
          content: ''
        });
        break;
      case 'happy':
        this.res.data.note.doc.body.sections.push({
          type: 'markdown',
          content: `
          \`\`\`
          ERROR ReferenceError: happiness is not defined
          at NoteEditorComponent.addSection (note-editor.component.ts:113)
          at Object.eval [as handleEvent] (NoteEditorComponent.html:88)
          at handleEvent (core.js:38098)
          at callWithDebugContext (core.js:39716)
          at Object.debugHandleEvent [as handleEvent] (core.js:39352)
          at dispatchEvent (core.js:25818)
          at core.js:37030
          at HTMLAnchorElement.<anonymous> (platform-browser.js:1789)
          at ZoneDelegate.invokeTask (zone-evergreen.js:391)
          at Object.onInvokeTask (core.js:34182)
          \`\`\`
          `
        });
        break;
    }
    this.calculatePercUsed();
  }

  deleteSection(sectionIndex: number) {
    this.invalidateSavedState();
    this.res.data.note.doc.body.sections.splice(sectionIndex, 1);
    this.calculatePercUsed();
  }

  onSectionChange() {
    this.invalidateSavedState();
    console.log('change', this.res.data.note.doc);
    this.calculatePercUsed();
  }

  calculatePercUsed() {
    const rawUsedPerc = JSON.stringify(this.res.data.note).length / this.lengthLimit * 100;
    this.percUsed = Math.floor(rawUsedPerc) ;
  }

  exit() {
    this.router.navigateByUrl('/n/' + this.noteId);
  }

  hotkeys(event) {
    // if (this.wasKeyDown) { return; }

    for (const hotkey of this.hotkeysData) {
      if (
        (event.code === hotkey.code || event.key === hotkey.key) &&
        event.ctrlKey === !!hotkey.ctrl &&
        event.altKey === !!hotkey.alt &&
        event.shiftKey === !!hotkey.shift
      ) {
        if (event.srcElement.localName === 'input' && !(hotkey.canInput || !!hotkey.ctrl || !!hotkey.alt)) { return; }
        if (!!event.srcElement.attributes.contenteditable && !(hotkey.canInput || !!hotkey.ctrl || !!hotkey.alt)) { return; }

        let res = hotkey.handler();

        if (res === undefined) {
          event.preventDefault();
        }
        return;
      }
    }
  }

  swapSection(index1: number, index2: number) {
    const tmpSection = this.res.data.note.doc.body.sections[index1];
    this.res.data.note.doc.body.sections[index1] = this.res.data.note.doc.body.sections[index2];
    this.res.data.note.doc.body.sections[index2] = tmpSection;
    this.invalidateSavedState(); // Invalidate saved state since the order of sections changed
  }
}
