import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Color } from 'ng2-charts';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {
  // Standardized api implement
  res: any; // {success, data/error}
  fetching = false; // Still in progress...

  requestCountChartOptions: any;
  noteCountChartOptions: any;
  userCountChartOptions: any;
  taskCountChartOptions: any;

  constructor(private api: ApiService, private settings: SettingsService) {
  }

  ngOnInit() {
    this.fetchData();
  }

  async fetchData() {
    this.fetching = true;
    this.res = await this.api.request('stats/get', {});

    if (!this.res.success) {
      this.fetching = false;
      return;
    }

    let timestamps = [];
    let requestCountData = [];
    let noteCountData = [];
    let userCountData = [];
    let taskCountData = [];
    for (const statEntry of this.res.data.stats) {
      timestamps.push(statEntry.timestamp);
      requestCountData.push(statEntry.requests.count);
      noteCountData.push(statEntry.counts.noteCount);
      userCountData.push(statEntry.counts.userCount);
      taskCountData.push(statEntry.counts.taskCount);
    }

    timestamps = timestamps.reverse();
    requestCountData = requestCountData.reverse();
    noteCountData = noteCountData.reverse();
    userCountData = userCountData.reverse();
    taskCountData = taskCountData.reverse();

    this.requestCountChartOptions = this.makeChartOptions('Requests', timestamps, requestCountData);
    this.noteCountChartOptions = this.makeChartOptions('Notes', timestamps, noteCountData);
    this.userCountChartOptions = this.makeChartOptions('Users', timestamps, userCountData);
    this.taskCountChartOptions = this.makeChartOptions('Tasks', timestamps, taskCountData);

    this.fetching = false;
  }

  makeChartOptions(name: string, timestamps: Array<number>, data: Array<number>) {
    const timeLabels = [];

    for (const timestamp of timestamps) {
      timeLabels.push(this.calcDateDeltaString(timestamp));
    }

    return {
      tooltip: {},
      xAxis: {
        data: timeLabels,
        silent: false,
        splitLine: {
          show: false
        }
      },
      yAxis: {
      },
      series: [{
        name,
        type: 'line',
        stack: 'counts',
        areaStyle: { normal: {} },
        data,
        animationDelay: (idx) => {
          return idx * 10;
        }
      }],
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => {
        return idx * 5;
      }
    };
  }

  calcDateDeltaString(time) {
    time = new Date().getTime() - time; // Get delta

    const minutes = time / 60 / 1000;
    const hours = minutes / 60;
    const days = hours / 24;

    let final;
    if (Math.floor(minutes) > 1) { final = Math.floor(minutes) + ' minutes ago'; }
    if (Math.floor(minutes) === 1) { final = Math.floor(minutes) + ' minute ago'; }

    if (Math.floor(hours) > 1) { final = Math.floor(hours) + ' hours ago'; }
    if (Math.floor(hours) === 1) { final = Math.floor(hours) + ' hour ago'; }

    if (Math.floor(days) > 1) { final = Math.floor(days) + ' days ago'; }
    if (Math.floor(days) === 1) { final = Math.floor(days) + ' day ago'; }

    if (!final) { final = 'a few seconds ago'; }

    return final;
  }

}
